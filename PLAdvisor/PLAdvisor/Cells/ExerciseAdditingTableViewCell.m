//
//  ExerciseAdditingTableViewCell.m
//  PLAdvisor
//
//  Created by Admin on 06.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ExerciseAdditingTableViewCell.h"

@implementation ExerciseAdditingTableViewCell 

- (void)awakeFromNib {
    [super awakeFromNib];
    [self dropDownConditions:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - Actions for DropDown Selection
- (void)actionWithEachSelect:(NSString *)textForLabel{
    [self dropDownConditions:NO];
    self.exercisesLabel.text = textForLabel;
}

- (IBAction)cellDidTapDropDown:(id)sender {
    if (self.benchPressSelection.isEnabled ==NO){
        [self dropDownConditions:YES];
    }else {
        [self dropDownConditions:NO];
    }
    if ([self.delegate respondsToSelector:@selector(didTapDropDown:)]) {
        [self.delegate didTapDropDown:self];
    }
}

#pragma mark - Conditions for DropDowns
- (void)dropDownConditions: (BOOL)isEnabled{
    [self.benchPressSelection setEnabled:isEnabled];
    [self.deadliftSelection setEnabled:isEnabled];
    [self.squadSelection setEnabled:isEnabled];
    [self.militaryPressSelection setEnabled:isEnabled];
    [self.bicepsHammerSelection setEnabled:isEnabled];
    [self.legPressSelection setEnabled:isEnabled];
    [self.pullDownSelection setEnabled:isEnabled];
    [self.benchPressNarrowSelection setEnabled:isEnabled];
    
    if (isEnabled){
        [self.benchPressSelection setBackgroundColor:[UIColor blackColor]];
        [self.benchPressSelection setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        [self.deadliftSelection setBackgroundColor:[UIColor darkGrayColor]];
        [self.deadliftSelection setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        [self.squadSelection setBackgroundColor:[UIColor grayColor]];
        [self.squadSelection setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        [self.militaryPressSelection setBackgroundColor:[UIColor whiteColor]];
        [self.militaryPressSelection setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        [self.bicepsHammerSelection setBackgroundColor:[UIColor blackColor]];
        [self.bicepsHammerSelection setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        [self.legPressSelection setBackgroundColor:[UIColor darkGrayColor]];
        [self.legPressSelection setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        [self.pullDownSelection setBackgroundColor:[UIColor grayColor]];
        [self.pullDownSelection setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        [self.benchPressNarrowSelection setBackgroundColor:[UIColor lightGrayColor]];
        [self.benchPressNarrowSelection setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
    }else{
        [self.benchPressSelection setBackgroundColor:[UIColor whiteColor]];
        [self.benchPressSelection setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.deadliftSelection setBackgroundColor:[UIColor whiteColor]];
        [self.deadliftSelection setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.squadSelection setBackgroundColor:[UIColor whiteColor]];
        [self.squadSelection setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.militaryPressSelection setBackgroundColor:[UIColor whiteColor]];
        [self.militaryPressSelection setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.bicepsHammerSelection setBackgroundColor:[UIColor whiteColor]];
        [self.bicepsHammerSelection setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.legPressSelection setBackgroundColor:[UIColor whiteColor]];
        [self.legPressSelection setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.pullDownSelection setBackgroundColor:[UIColor whiteColor]];
        [self.pullDownSelection setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.benchPressNarrowSelection setBackgroundColor:[UIColor whiteColor]];
        [self.benchPressNarrowSelection setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

#pragma mark - Selecting Actions of DropDown
- (IBAction)cellDidSelectBenchPress:(id)sender {
    [self actionWithEachSelect:self.benchPressSelection.titleLabel.text];
    self.exerciseImage.image = [UIImage imageNamed:@"BenchPress.jpg"];
}

- (IBAction)cellDidSelectDeadlift:(id)sender {
    [self actionWithEachSelect:self.deadliftSelection.titleLabel.text];
    self.exerciseImage.image = [UIImage imageNamed:@"Deadlift.jpg"];
}

- (IBAction)cellDidSelectSquad:(id)sender {
    [self actionWithEachSelect:self.squadSelection.titleLabel.text];
    self.exerciseImage.image = [UIImage imageNamed:@"Squat.jpg"];
}

- (IBAction)cellDidSelectMilitary:(id)sender {
    [self actionWithEachSelect:self.militaryPressSelection.titleLabel.text];
    self.exerciseImage.image = [UIImage imageNamed:@"MilitaryPress.jpg"];
}

- (IBAction)cellDidSelectBiceps:(id)sender {
    [self actionWithEachSelect:self.bicepsHammerSelection.titleLabel.text];
    self.exerciseImage.image = [UIImage imageNamed:@"BicepsHammer.jpg"];
}

- (IBAction)cellDidSelectLegPress:(id)sender {
    [self actionWithEachSelect:self.legPressSelection.titleLabel.text];
    self.exerciseImage.image = [UIImage imageNamed:@"LegPress.jpg"];
}

- (IBAction)cellDidSelectPullDown:(id)sender {
    [self actionWithEachSelect:self.pullDownSelection.titleLabel.text];
    self.exerciseImage.image = [UIImage imageNamed:@"PullDown.jpg"];
}

- (IBAction)cellDidSelectBenchPressNarrow:(id)sender {
    [self actionWithEachSelect:self.benchPressNarrowSelection.titleLabel.text];
    self.exerciseImage.image = [UIImage imageNamed:@"BenchPressNarrow.jpg"];
}


@end
