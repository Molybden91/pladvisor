//
//  ExerciseAdditingTableViewCell.h
//  PLAdvisor
//
//  Created by Admin on 06.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Exercise+CoreDataClass.h"

@class ExerciseAdditingTableViewCell;
@protocol ExerciseAdditingTableViewCellDelegate <NSObject>

@optional
- (void)didTapDropDown: (ExerciseAdditingTableViewCell *)cell;

@end

@interface ExerciseAdditingTableViewCell : UITableViewCell 

@property (nonatomic, weak) id <ExerciseAdditingTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *repsTextField;
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;

@property (weak, nonatomic) IBOutlet UIButton *exercisesDropDown;
@property (weak, nonatomic) IBOutlet UILabel *exercisesLabel;

@property (weak, nonatomic) IBOutlet UIView *exerciseDropDownView;

@property (weak, nonatomic) IBOutlet UIButton *benchPressSelection;
@property (weak, nonatomic) IBOutlet UIButton *deadliftSelection;
@property (weak, nonatomic) IBOutlet UIButton *squadSelection;
@property (weak, nonatomic) IBOutlet UIButton *militaryPressSelection;
@property (weak, nonatomic) IBOutlet UIButton *benchPressNarrowSelection;
@property (weak, nonatomic) IBOutlet UIButton *legPressSelection;
@property (weak, nonatomic) IBOutlet UIButton *pullDownSelection;
@property (weak, nonatomic) IBOutlet UIButton *bicepsHammerSelection;

@property (weak, nonatomic) IBOutlet UIImageView *exerciseImage;

@end
