//
//  TrainingDayTableViewCell.m
//  PLAdvisor
//
//  Created by Admin on 18.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "TrainingDayTableViewCell.h"
#import "Exercise+CoreDataClass.h"

@implementation TrainingDayTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.exerciseTypeLabel setEnabled:NO];
    [self .repsLabel setEnabled:NO];
    [self .weightLabel setEnabled:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)configureTrainingDayCell: (Exercise *)exercise{
    self.exerciseTypesTextField.text = exercise.type;
    self.repsTextField.text = [[NSNumber numberWithInt:exercise.reps]  stringValue];
    self.weightTextField.text = [[NSNumber numberWithInt:exercise.weight]  stringValue];;
}


@end
