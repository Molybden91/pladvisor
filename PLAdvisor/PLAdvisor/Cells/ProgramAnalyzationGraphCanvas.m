//
//  ProgramAnalyzationGraphCanvas.m
//  PLAdvisor
//
//  Created by Admin on 30.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ProgramAnalyzationGraphCanvas.h"
#import "TrainingDayHelper.h"

static CGFloat const kArrowLength = 20;
static CGFloat const kEdgeOffset = 60;
static CGFloat const kTopOffset = 40;
static CGFloat const kGridSize = 10;

@implementation ProgramAnalyzationGraphCanvas
#pragma mark - Initialization
- (instancetype) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self configure];
    }
    return self;
}

#pragma mark - Configuration
- (void)configure{
    self.backgroundColor = [UIColor whiteColor];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.layer.cornerRadius = 100;
    self.layer.cornerRadius = self.bounds.size.width/2;
}

#pragma mark - graph drawing
- (void) drawRect:(CGRect)rect{
    [self drawGraph:rect];
}

#pragma mark - graph elements drawing 
- (void)drawCurveInContext: (CGContextRef)contexRef inRect: (CGRect)rect{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGColorRef color = [[UIColor blackColor]  CGColor];
    CGContextSetStrokeColorWithColor(context, color);
    CGContextSetLineWidth(context, 5.0);
    CGContextMoveToPoint(context, CGPointZero.x, CGPointZero.y);
    CGContextAddLineToPoint(context, rect.size.width, rect.size.height/2);
    CGContextAddLineToPoint(context, rect.size.width/2, rect.size.height);
    CGContextDrawPath(context, kCGPathStroke);
    
}

- (void)drawGraph: (CGRect)rect{
    [self drawAxis:rect];
    [self drawGridLines:rect];
    [self drawGraphLines:rect];
}

- (void)drawAxis: (CGRect) rect{
    CGFloat arrowOffset =sqrtf(powf(kArrowLength, 2) /2);
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    aPath.lineWidth = 5.0;
    [[UIColor blackColor] setStroke];
    
    CGPoint startPoint = CGPointMake(kEdgeOffset, 2*kTopOffset);
    CGPoint pointTwo = CGPointMake(kEdgeOffset, rect.size.height-2*kTopOffset);
    CGPoint pointThird = CGPointMake(rect.size.width - 0.5*kEdgeOffset, rect.size.height - 2*kTopOffset);
    
    CGPoint pointFirstRightArrow = CGPointMake(rect.size.width - 0.5*kEdgeOffset-arrowOffset, rect.size.height - 2*kTopOffset - arrowOffset);
    CGPoint pointSecondRightArrow = CGPointMake(rect.size.width - 0.5*kEdgeOffset-arrowOffset, rect.size.height - 2*kTopOffset + arrowOffset);
    
    CGPoint pointFirstTopArrow = CGPointMake(kEdgeOffset + arrowOffset, 2*kTopOffset + arrowOffset);
    CGPoint pointSecondTopArrow = CGPointMake(kEdgeOffset - arrowOffset, 2*kTopOffset + arrowOffset);
    
    [aPath moveToPoint:startPoint];
    [aPath addLineToPoint:pointTwo];
    [aPath moveToPoint:pointTwo];
    [aPath addLineToPoint:pointThird];
    
    [aPath moveToPoint:pointThird];
    [aPath addLineToPoint:pointFirstRightArrow];
    [aPath moveToPoint:pointThird];
    [aPath addLineToPoint:pointSecondRightArrow];
    
    [aPath moveToPoint:startPoint];
    [aPath addLineToPoint:pointFirstTopArrow];
    [aPath moveToPoint:startPoint];
    [aPath addLineToPoint:pointSecondTopArrow];
    
    [aPath closePath];
    [aPath stroke];
}

- (void)drawGridLines: (CGRect) rect{
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    aPath.lineWidth= 5.0;
    [[UIColor blackColor] setStroke];
    
    CGFloat numberOfGridSteps = 10;
    CGFloat gridStepOY = rect.size.height/numberOfGridSteps;
    CGFloat gridStepOX = (rect.size.width - 0.5*kEdgeOffset)/numberOfGridSteps;
    
    for(float stepOY=2*kTopOffset+gridStepOY; stepOY<rect.size.height-2*kTopOffset; stepOY+=gridStepOY ){
        CGPoint startPoint = CGPointMake(kEdgeOffset, stepOY);
        [aPath moveToPoint:startPoint];
        CGPoint endPoint = CGPointMake(kEdgeOffset+kGridSize, stepOY);
        [aPath addLineToPoint:endPoint];
    }
    
    for(float stepOX=kEdgeOffset; stepOX<rect.size.width - kEdgeOffset; stepOX+=gridStepOX ){
        CGPoint startPoint = CGPointMake(stepOX, rect.size.height-2*kTopOffset);
        [aPath moveToPoint:startPoint];
        CGPoint endPoint = CGPointMake(stepOX, rect.size.height-2*kTopOffset-kGridSize);
        [aPath addLineToPoint:endPoint];
    }
    
    [aPath closePath];
    [aPath stroke];
}

- (void)drawGraphLines: (CGRect) rect{
   // TrainingDayHelper *helper = [TrainingDayHelper new];
    //NSMutableDictionary <NSString*, NSNumber*> *benchPress = [helper prepareExercieseForPlot];
    
    UIBezierPath *aPathBenchPress = [UIBezierPath bezierPath];
    aPathBenchPress.lineWidth= 3.0;
    [[UIColor redColor] setStroke];
    
//    if (benchPress.count<=7){
//        for (NSString * key in benchPress.allKeys) {
//            
//        }
//    }
    

}

@end
