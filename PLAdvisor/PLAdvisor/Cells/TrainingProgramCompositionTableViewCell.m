//
//  TrainingProgramCompositionTableViewCell.m
//  PLAdvisor
//
//  Created by Admin on 06.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "TrainingProgramCompositionTableViewCell.h"
@interface TrainingProgramCompositionTableViewCell ()

//TextField just for captions
@property (weak, nonatomic) IBOutlet UITextField *captureOwnHeight;
@property (weak, nonatomic) IBOutlet UITextField *captureOwnWeight;
@property (weak, nonatomic) IBOutlet UITextField *captureBenchPress;
@property (weak, nonatomic) IBOutlet UITextField *captureDeadlift;
@property (weak, nonatomic) IBOutlet UITextField *captureSquad;
@property (weak, nonatomic) IBOutlet UITextField *captureMilitaryPress;





@end

@implementation TrainingProgramCompositionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.captureOwnHeight  setEnabled:NO];
    [self.captureOwnWeight setEnabled:NO];
    [self.captureSquad setEnabled:NO];
    [self.captureDeadlift setEnabled:NO];
    [self.captureBenchPress setEnabled:NO];
    [self.captureMilitaryPress setEnabled:NO];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configurateCell:(InitialCapabilities *)initialCapabilities{
    
}



@end
