//
//  AnalyzeDataTableViewCell.h
//  PLAdvisor
//
//  Created by Admin on 07.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainingDay+CoreDataClass.h"

@class AnalyzeDataTableViewCell;

@protocol AnalyzedataTableViewCellDelegate <NSObject>

- (void)didTapTrainingDayButton: (AnalyzeDataTableViewCell *)cell;

@end

@interface AnalyzeDataTableViewCell : UITableViewCell

@property (nonatomic, weak) id <AnalyzedataTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *trainingDayTextField;

- (void)configureCell: (TrainingDay *) trainingDay;

- (TrainingDay *)insertTrainingDay;

@end
