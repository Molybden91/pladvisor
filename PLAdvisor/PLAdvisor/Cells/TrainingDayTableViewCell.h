//
//  TrainingDayTableViewCell.h
//  PLAdvisor
//
//  Created by Admin on 18.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Exercise+CoreDataClass.h"

@interface TrainingDayTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *exerciseTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *repsLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;


@property (weak, nonatomic) IBOutlet UITextField *exerciseTypesTextField;
@property (weak, nonatomic) IBOutlet UITextField *repsTextField;
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;

- (void)configureTrainingDayCell: (Exercise *)exercise;

@end
