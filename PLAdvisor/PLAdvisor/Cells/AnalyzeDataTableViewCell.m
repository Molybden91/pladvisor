//
//  AnalyzeDataTableViewCell.m
//  PLAdvisor
//
//  Created by Admin on 07.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AnalyzeDataTableViewCell.h"
#import "TrainingDayHelper.h"

@implementation AnalyzeDataTableViewCell 

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.trainingDayTextField  setTextColor:[UIColor blueColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)didTapButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didTapTrainingDayButton:)]) {
        [self.delegate didTapTrainingDayButton:self];
    }
}

- (void)configureCell: (TrainingDay *) trainingDay{
    self.trainingDayTextField.text = trainingDay.dateOfExecution;
}

- (TrainingDay *)insertTrainingDay{
    TrainingDayHelper *trainingDay = [TrainingDayHelper new];
    for (TrainingDay *day in trainingDay.trainingDaysFromBase) {
        if ([day.dateOfExecution isEqualToString:self.trainingDayTextField.text]){
            return day;
        }
    }
    return nil;
}
@end
