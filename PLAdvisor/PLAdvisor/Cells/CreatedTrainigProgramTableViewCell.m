//
//  CreatedTrainigProgramTableViewCell.m
//  PLAdvisor
//
//  Created by Admin on 06.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "CreatedTrainigProgramTableViewCell.h"

@implementation CreatedTrainigProgramTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)configureCell:(NSArray <Exercise *>*)exercises{
    self.firstExerciseTypeTextField.text = exercises[0].type;
    self.firstExerciseRepsTextField.text = [[NSNumber numberWithInt: exercises[0]. reps] stringValue];
    self.firstExerciseWeightTextField.text = [[NSNumber numberWithInt:exercises[0].weight] stringValue];
    
    self.secondExerciseTypeTextField.text = exercises[1].type;
    self.secondexerciseRepsTextField.text = [[NSNumber numberWithInt: exercises[1].reps] stringValue];
    self.secondExerciseWeightTextField.text = [[NSNumber numberWithInt:exercises[1].weight] stringValue];
    
    self.thirdExerciseTypeTextField.text = exercises[2].type;
    self.thirdExerciseRepsTextField.text = [[NSNumber numberWithInt: exercises[2].reps] stringValue];
    self.thirdExerciseWeightTextField.text = [[NSNumber numberWithInt:exercises[2].weight] stringValue];
    
    if (exercises.count>3){
        self.fourthExerciseTypeTextField.text = exercises[3].type;
        self.fourthExerciseRepsTextField.text = [[NSNumber numberWithInt: exercises[3].reps] stringValue];
        self.fourthExerciseWeightTextField.text = [[NSNumber numberWithInt:exercises[3].weight] stringValue];
        
        self.fifthExerciseTypeTextField.text = exercises[4].type;
        self.fifthExerciseRepsTextField.text = [[NSNumber numberWithInt: exercises[4].reps] stringValue];
        self.fifthExerciseWeightTextField.text = [[NSNumber numberWithInt:exercises[4].weight] stringValue];
    }else{
        self.fourthExerciseTypeTextField.text = @"Not defined";
        self.fourthExerciseRepsTextField.text = @"Not defined";
        self.fourthExerciseWeightTextField.text = @"Not defined";
        
        self.fifthExerciseTypeTextField.text = @"Not defined";
        self.fifthExerciseRepsTextField.text = @"Not defined";
        self.fifthExerciseWeightTextField.text = @"Not defined";
    }
}



@end
