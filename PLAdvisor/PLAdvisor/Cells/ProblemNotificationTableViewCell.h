//
//  ProblemNotificationTableViewCell.h
//  PLAdvisor
//
//  Created by Admin on 16.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProblemNotificationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *problemWithTextField;
@property (weak, nonatomic) IBOutlet UILabel *dropDownLabel;
@property (weak, nonatomic) IBOutlet UIButton *dropDownButton;
@property (weak, nonatomic) IBOutlet UIButton *injurySelection;
@property (weak, nonatomic) IBOutlet UIButton *illnessSelection;
@property (weak, nonatomic) IBOutlet UIButton *benchPressSelection;
@property (weak, nonatomic) IBOutlet UIButton *deadliftSelection;
@property (weak, nonatomic) IBOutlet UIButton *squatSelection;

@property (weak, nonatomic) IBOutlet UILabel *injuryLabel;
@property (weak, nonatomic) IBOutlet UILabel *illnessLabel;
@property (weak, nonatomic) IBOutlet UILabel *benchPressLabel;
@property (weak, nonatomic) IBOutlet UILabel *deadliftLabel;
@property (weak, nonatomic) IBOutlet UILabel *squatLabel;



@property (weak, nonatomic) IBOutlet UITextField *concreteProblemTextField;
@property (weak, nonatomic) IBOutlet UILabel *concreteDropDownLabel;
@property (weak, nonatomic) IBOutlet UIButton *dropDownConcreteProblem;
@property (weak, nonatomic) IBOutlet UIButton *firstSelection;
@property (weak, nonatomic) IBOutlet UIButton *secondSelection;
@property (weak, nonatomic) IBOutlet UIButton *thirdSelection;
@property (weak, nonatomic) IBOutlet UIButton *fourthSelection;
@property (weak, nonatomic) IBOutlet UIButton *fifthSelection;

@property (weak, nonatomic) IBOutlet UILabel *firstSelectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondSelectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdSelectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *fourthSelectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *fifthSelectionLabel;


@property (weak, nonatomic) IBOutlet UIDatePicker *dateOfProblem;


@end
