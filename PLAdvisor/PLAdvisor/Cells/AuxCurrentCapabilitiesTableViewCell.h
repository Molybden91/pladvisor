//
//  AuxCurrentCapabilitiesTableViewCell.h
//  PLAdvisor
//
//  Created by Admin on 26.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Exercise+CoreDataClass.h"

@interface AuxCurrentCapabilitiesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *exerciseTypeLabel;
@property (weak, nonatomic) IBOutlet UITextField *exerciseTextField;

@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;

- (void) configureCell:(Exercise*) exercise;

@end
