//
//  toDiaryButton.m
//  PLAdvisor
//
//  Created by Admin on 17.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "CentralNavigationButton.h"

@implementation CentralNavigationButton

- (instancetype)initWithTitle:(NSString *)buttonTitle{
    self = [super init];
    if (self){
        self = (CentralNavigationButton *)[self configureButton:buttonTitle];
    }
    return self;
}

- (UIButton *)configureButton:(NSString *)title{
    UIButton *titleButton = [UIButton new];
    [titleButton setTitle:title forState:UIControlStateNormal];
    [titleButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    titleButton.frame = CGRectMake(270, 10, 80, 30);
    titleButton.userInteractionEnabled = YES;
    return titleButton;
}

@end
