//
//  AuxCurrentCapabilitiesTableViewCell.m
//  PLAdvisor
//
//  Created by Admin on 26.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AuxCurrentCapabilitiesTableViewCell.h"

@implementation AuxCurrentCapabilitiesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.exerciseTypeLabel.enabled = NO;
    self.weightLabel.enabled = NO;
    self.exerciseTextField.enabled = NO;
    self.weightTextField.enabled = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void) configureCell:(Exercise*) exercise{
    self.exerciseTextField.text = exercise.type;
    self.weightTextField.text = [[NSNumber numberWithInt:exercise.weight]  stringValue];
    NSLog(@"%@",self.weightTextField.text);
}
@end
