//
//  toDiaryButton.h
//  PLAdvisor
//
//  Created by Admin on 17.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CentralNavigationButton : UIButton

- (instancetype)initWithTitle:(NSString *)buttonTitle;

- (UIButton *)configureButton:(NSString *)title;

@end
