//
//  doneButton.m
//  PLAdvisor
//
//  Created by Admin on 17.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "RightNavigationButton.h"

@implementation RightNavigationButton

- (instancetype)initWithTitle: (NSString *)buttonTitle{
    self = [super init];
    if (self){
        self = (RightNavigationButton *)[self configureButton:buttonTitle];
    }
    return self;
}

- (UIButton *)configureButton:(NSString *)title{
    UIButton *rightButton = [UIButton new];
    [rightButton  setTitle:title forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor colorWithRed:0.0 green:0.0 blue:250.0 alpha:1] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(270, 10, 80, 30);
    rightButton.userInteractionEnabled = YES;
    return rightButton;
}
@end
