//
//  ProblemNotificationTableViewCell.m
//  PLAdvisor
//
//  Created by Admin on 16.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ProblemNotificationTableViewCell.h"

@implementation ProblemNotificationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self dropDownCondition:NO];
    [self concreteDropDownCondition:NO];
    [self.problemWithTextField setEnabled:NO];
    [self.concreteProblemTextField setEnabled:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
#pragma mark - Actions for DropDown Selection
- (void)actionForEachSelectionDropDown:(NSString *)textForLabel{
    [self dropDownCondition:NO];
    self.dropDownButton.titleLabel.text = @"";
    self.dropDownLabel.text = textForLabel;
}

- (void)actionForEachSelectionConcreteDropDown: (NSString *)textForLabel{
    [self concreteDropDownCondition:NO];
    self.dropDownConcreteProblem.titleLabel.text = @"";
    self.concreteDropDownLabel.text = textForLabel;
}

#pragma mark - Conditions for DropDowns
- (void)dropDownCondition: (BOOL)isEnabled{
    [self.benchPressSelection setEnabled:isEnabled];
    [self.deadliftSelection setEnabled:isEnabled];
    [self.squatSelection setEnabled:isEnabled];
    [self.injurySelection setEnabled:isEnabled];
    [self.illnessSelection setEnabled:isEnabled];
    
    if(isEnabled){
        [self.injurySelection setBackgroundColor:[UIColor blackColor]];
        [self.injuryLabel setTextColor:[UIColor blueColor]];
        
        [self.illnessSelection setBackgroundColor:[UIColor darkGrayColor]];
        [self.illnessLabel setTextColor:[UIColor blueColor]];
        
        [self.benchPressSelection setBackgroundColor:[UIColor grayColor]];
        [self.benchPressLabel setTextColor:[UIColor blueColor]];
        
        [self.deadliftSelection setBackgroundColor:[UIColor lightGrayColor]];
        [self.deadliftLabel setTextColor:[UIColor blueColor]];
        
        [self.squatSelection setBackgroundColor:[UIColor whiteColor]];
        [self.squatLabel setTextColor:[UIColor blueColor]];
        
    }else {
        [self.injurySelection setBackgroundColor:[UIColor whiteColor]];
        [self.injuryLabel setTextColor:[UIColor whiteColor]];
        
        [self.illnessSelection setBackgroundColor:[UIColor whiteColor]];
        [self.illnessLabel setTextColor:[UIColor whiteColor]];
        
        [self.benchPressSelection setBackgroundColor:[UIColor whiteColor]];
        [self.benchPressLabel setTextColor:[UIColor whiteColor]];
        
        [self.deadliftSelection setBackgroundColor:[UIColor whiteColor]];
        [self.deadliftLabel setTextColor:[UIColor whiteColor]];
        
        [self.squatSelection setBackgroundColor:[UIColor whiteColor]];
        [self.squatLabel setTextColor:[UIColor whiteColor]];
    }
}

- (void)concreteDropDownCondition: (BOOL)isEnabled{
    [self.firstSelection setEnabled:isEnabled];
    [self.secondSelection setEnabled:isEnabled];
    [self.thirdSelection setEnabled:isEnabled];
    [self.fourthSelection setEnabled:isEnabled];
    [self.fifthSelection setEnabled:isEnabled];
    
    if (isEnabled){
        [self.firstSelection setBackgroundColor:[UIColor blackColor]];
        [self.firstSelectionLabel setTextColor:[UIColor blueColor]];
        
        [self.secondSelection setBackgroundColor:[UIColor darkGrayColor]];
        [self.secondSelectionLabel setTextColor:[UIColor blueColor]];
        
        [self.thirdSelection setBackgroundColor:[UIColor grayColor]];
        [self.thirdSelectionLabel setTextColor:[UIColor blueColor]];
        
        [self.fourthSelection setBackgroundColor:[UIColor lightGrayColor]];
        [self.fourthSelectionLabel setTextColor:[UIColor blueColor]];
        
        [self.fifthSelection setBackgroundColor:[UIColor whiteColor]];
        [self.fifthSelectionLabel setTextColor:[UIColor blueColor]];
        
    }else {
        [self.firstSelection setBackgroundColor:[UIColor whiteColor]];
        [self.firstSelectionLabel setTextColor:[UIColor whiteColor]];
        
        [self.secondSelection setBackgroundColor:[UIColor whiteColor]];
        [self.secondSelectionLabel setTextColor:[UIColor whiteColor]];
        
        [self.thirdSelection setBackgroundColor:[UIColor whiteColor]];
        [self.thirdSelectionLabel setTextColor:[UIColor whiteColor]];
        
        [self.fourthSelection setBackgroundColor:[UIColor whiteColor]];
        [self.fourthSelectionLabel setTextColor:[UIColor whiteColor]];
        
        [self.fifthSelection setBackgroundColor:[UIColor whiteColor]];
        [self.fifthSelectionLabel setTextColor:[UIColor whiteColor]];
    }
}


// for UIView Problem Notification

#pragma mark - Selecting Actions of DropDown
- (IBAction)didTapDropDown:(id)sender {
    if (self.benchPressSelection.isEnabled ==NO){
        [self dropDownCondition:YES];
    }else {
        [self dropDownCondition:NO];
    }
}

- (IBAction)didInjurySelected:(id)sender {
    [self actionForEachSelectionDropDown:self.injuryLabel.text];
    self.firstSelectionLabel.text = @"leg";
    self.secondSelectionLabel.text = @"arm";
    self.thirdSelectionLabel.text = @"wrist";
    self.fourthSelectionLabel.text = @"back";
    self.fifthSelectionLabel.text =@"shoulder";
    self.concreteDropDownLabel.text = @"";
    self.dropDownConcreteProblem.titleLabel.text = @"▼";
}

- (IBAction)didIlnessSelected:(id)sender {
    [self actionForEachSelectionDropDown:self.illnessLabel.text];
    self.concreteDropDownLabel.text = @"";
    self.dropDownConcreteProblem.titleLabel.text = @"▼";
}

- (IBAction)didBenchPressSelected:(id)sender {
    [self actionForEachSelectionDropDown:self.benchPressLabel.text];
    self.firstSelectionLabel.text = @"start phase";
    self.secondSelectionLabel.text = @"medium phase";
    self.thirdSelectionLabel.text = @"booster phase";
    self.concreteDropDownLabel.text = @"";
    self.dropDownConcreteProblem.titleLabel.text = @"▼";
}

- (IBAction)didDeadliftSelected:(id)sender {
    [self actionForEachSelectionDropDown:self.deadliftLabel.text];
    self.firstSelectionLabel.text = @"start phase";
    self.secondSelectionLabel.text = @"booster phase";
    self.concreteDropDownLabel.text = @"";
}

- (IBAction)didSquatSelected:(id)sender {
    [self actionForEachSelectionDropDown:self.squatLabel.text];
    self.firstSelectionLabel.text = @"start phase";
    self.secondSelectionLabel.text = @"booster phase";
    self.concreteDropDownLabel.text = @"";
    self.dropDownConcreteProblem.titleLabel.text = @"▼";
}

// for UIView Concrete Notification
- (void)setAllSelectionVisible{
    self.firstSelectionLabel.textColor = [UIColor blueColor];
    self.firstSelection.backgroundColor = [UIColor blackColor];
    
    self.secondSelectionLabel.textColor = [UIColor blueColor];
    self.secondSelection.backgroundColor = [UIColor darkGrayColor];
    
    self.thirdSelectionLabel.textColor = [UIColor blueColor];
    self.thirdSelection.backgroundColor = [UIColor grayColor];
    
    self.fourthSelectionLabel.textColor = [UIColor blueColor];
    self.fourthSelection.backgroundColor = [UIColor lightGrayColor];
    
    self.fifthSelectionLabel.textColor = [UIColor blueColor];
    self.fifthSelection.backgroundColor = [UIColor whiteColor];
}

#pragma mark - Selecting Actions of Concrete DropDown
- (IBAction)didTapConcreteDropDown:(id)sender {
    if (self.firstSelection.isEnabled ==NO){
        [self concreteDropDownCondition:YES];
        [self conditionOfDropDownElementsWhenDisabled];
    }else {
        [self concreteDropDownCondition:NO];
        [self conditionOfDropDownElementsWhenEnabled];
    }
}

- (IBAction)didFirstPositionSelected:(id)sender {
    [self actionForEachSelectionConcreteDropDown:self.firstSelectionLabel.text];
}

- (IBAction)didSecondPositionSelected:(id)sender {
    [self actionForEachSelectionConcreteDropDown:self.secondSelectionLabel.text];
}

- (IBAction)didThirdPositionSelected:(id)sender {
    [self actionForEachSelectionConcreteDropDown:self.thirdSelectionLabel.text];
}

- (IBAction)didFourthPositionSelected:(id)sender {
    [self actionForEachSelectionConcreteDropDown:self.fourthSelectionLabel.text];
}

- (IBAction)didFifthPositionSelected:(id)sender {
    [self actionForEachSelectionConcreteDropDown:self.fifthSelectionLabel.text];
}

#pragma mark - Conditions of DropDown When Enabled
- (void)conditionOfDropDownElementsWhenEnabled{
    self.firstSelection.backgroundColor = [UIColor whiteColor];
    self.firstSelectionLabel.textColor = [UIColor whiteColor];
    
    self.secondSelection.backgroundColor = [UIColor whiteColor];
    self.secondSelectionLabel.textColor = [UIColor whiteColor];
    
    self.thirdSelectionLabel.textColor = [UIColor whiteColor];
    self.thirdSelection.backgroundColor = [UIColor whiteColor];
    
    self.fourthSelectionLabel.textColor = [UIColor whiteColor];
    self.fourthSelection.backgroundColor = [UIColor whiteColor];
    
    self.fifthSelectionLabel.textColor = [UIColor whiteColor];
    self.fifthSelection.backgroundColor = [UIColor whiteColor];
}

- (void)conditionOfDropDownElementsWhenDisabled{
    if ([self.dropDownLabel.text isEqualToString:@"Deadlift Technique"]||[self.dropDownLabel.text isEqualToString:@"Squat Technique"]) {
        self.firstSelection.backgroundColor = [UIColor blackColor];
        self.firstSelectionLabel.textColor = [UIColor blueColor];
        
        self.secondSelection.backgroundColor = [UIColor darkGrayColor];
        self.secondSelectionLabel.textColor = [UIColor blueColor];
        
        self.thirdSelectionLabel.textColor = [UIColor whiteColor];
        self.thirdSelection.backgroundColor = [UIColor whiteColor];
        
        self.fourthSelectionLabel.textColor = [UIColor whiteColor];
        self.fourthSelection.backgroundColor = [UIColor whiteColor];
        
        self.fifthSelectionLabel.textColor = [UIColor whiteColor];
        self.fifthSelection.backgroundColor = [UIColor whiteColor];
    }else if ([self.dropDownLabel.text isEqualToString:@"Bench Press Technique"]){
        self.firstSelection.backgroundColor = [UIColor blackColor];
        self.firstSelectionLabel.textColor = [UIColor blueColor];
        
        self.secondSelection.backgroundColor = [UIColor darkGrayColor];
        self.secondSelectionLabel.textColor = [UIColor blueColor];
        
        self.thirdSelection.backgroundColor = [UIColor grayColor];
        self.thirdSelectionLabel.textColor = [UIColor blueColor];
        
        self.fourthSelectionLabel.textColor = [UIColor whiteColor];
        self.fourthSelection.backgroundColor = [UIColor whiteColor];
        
        self.fifthSelectionLabel.textColor = [UIColor whiteColor];
        self.fifthSelection.backgroundColor = [UIColor whiteColor];
    }else if ([self.dropDownLabel.text isEqualToString:@"Illness"]) {
        
        self.firstSelectionLabel.textColor = [UIColor whiteColor];
        self.firstSelection.backgroundColor = [UIColor whiteColor];
        
        self.secondSelectionLabel.textColor = [UIColor whiteColor];
        self.secondSelection.backgroundColor = [UIColor whiteColor];
        
        self.thirdSelectionLabel.textColor = [UIColor whiteColor];
        self.thirdSelection.backgroundColor = [UIColor whiteColor];
        
        self.fourthSelectionLabel.textColor = [UIColor whiteColor];
        self.fourthSelection.backgroundColor = [UIColor whiteColor];
        
        self.fifthSelectionLabel.textColor = [UIColor whiteColor];
        self.fifthSelection.backgroundColor = [UIColor whiteColor];
    }else {
        [self setAllSelectionVisible];
    }
    
}

@end
