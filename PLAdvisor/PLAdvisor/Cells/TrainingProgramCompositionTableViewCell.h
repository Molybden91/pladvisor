//
//  TrainingProgramCompositionTableViewCell.h
//  PLAdvisor
//
//  Created by Admin on 06.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InitialCapabilities+CoreDataClass.h"
#import "InitialCapabilities+CoreDataClass.h"

@interface TrainingProgramCompositionTableViewCell : UITableViewCell


/**
 Properties\ outlets for initial data for future training program of the athlet.
 */
@property (weak, nonatomic) IBOutlet UITextField *ownHeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *ownWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *benchPressTextField;
@property (weak, nonatomic) IBOutlet UITextField *deadliftTextField;
@property (weak, nonatomic) IBOutlet UITextField *squadTextField;
@property (weak, nonatomic) IBOutlet UITextField *militaryPressTextField;


- (void)configurateCell:(InitialCapabilities *)initialCapabilities;

@end
