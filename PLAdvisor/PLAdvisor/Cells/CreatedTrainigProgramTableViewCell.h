//
//  CreatedTrainigProgramTableViewCell.h
//  PLAdvisor
//
//  Created by Admin on 06.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TrainingDay+CoreDataClass.h"
#import "Exercise+CoreDataClass.h"

@interface CreatedTrainigProgramTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *firstExerciseTypeTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstExerciseWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstExerciseRepsTextField;

@property (weak, nonatomic) IBOutlet UITextField *secondExerciseTypeTextField;
@property (weak, nonatomic) IBOutlet UITextField *secondExerciseWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *secondexerciseRepsTextField;

@property (weak, nonatomic) IBOutlet UITextField *thirdExerciseTypeTextField;
@property (weak, nonatomic) IBOutlet UITextField *thirdExerciseWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *thirdExerciseRepsTextField;

@property (weak, nonatomic) IBOutlet UITextField *fourthExerciseTypeTextField;
@property (weak, nonatomic) IBOutlet UITextField *fourthExerciseWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *fourthExerciseRepsTextField;

@property (weak, nonatomic) IBOutlet UITextField *fifthExerciseTypeTextField;
@property (weak, nonatomic) IBOutlet UITextField *fifthExerciseWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *fifthExerciseRepsTextField;


- (void)configureCell:(NSArray <Exercise *>*)exercises;

@end
