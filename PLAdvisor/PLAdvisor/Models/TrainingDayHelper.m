//
//  TrainingDayHelper.m
//  PLAdvisor
//
//  Created by Admin on 18.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "TrainingDayHelper.h"
#import "CoreDataHelper.h"
#import "Constants.h"

@implementation TrainingDayHelper

- (NSArray <TrainingDay *>*)trainingDaysFromBase{
    if (_trainingDaysFromBase ==nil){
        _trainingDaysFromBase = [CoreDataHelper loadTrainingDays];
    }
    return _trainingDaysFromBase;
}

- (NSMutableArray <TrainingDay *>*)preparedForDisplay{
    if (_preparedForDisplay ==nil){
        _preparedForDisplay = [NSMutableArray new];
        [self prepareForDisplay];
    }
    return _preparedForDisplay;
}

- (void)prepareForDisplay{
    for (TrainingDay *day in self.trainingDaysFromBase) {
        if (!([day.dateOfExecution characterAtIndex:0]== 'D' && [day.dateOfExecution characterAtIndex:1]== 'a' && [day.dateOfExecution characterAtIndex:2]=='y')){
            [self.preparedForDisplay  addObject:day];
        }
    }
}
- (NSMutableDictionary<NSString*,Exercise*>*) prepareExercieseForPlot{
    NSMutableDictionary <NSString*,Exercise *>*exercisesDictionary = [NSMutableDictionary new];
    for(int i=0;i<self.preparedForDisplay.count;i++){
        NSSet<Exercise *> *exercises = self.preparedForDisplay[i].trainingDayExercises ;
        for (Exercise *exercise in exercises) {
            [exercisesDictionary setValue:exercise forKey:self.preparedForDisplay[i].dateOfExecution];
        }
    }
    for (NSString *key in exercisesDictionary.allKeys) {
        NSLog(@"%@-%@", [exercisesDictionary objectForKey:key],key);
    }
    return exercisesDictionary;
}

@end
