//
//  ProgramCreationHelper.m
//  PLAdvisor
//
//  Created by Admin on 19.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ProgramCreationHelper.h"
#import "CoreDataHelper.h"
#import "Exercise+CoreDataClass.h"
#import "Constants.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase/Firebase.h>
@interface ProgramCreationHelper ()

@property (nonatomic, strong) FIRDatabaseReference *ref;
@property NSString *uid;

@end

@implementation ProgramCreationHelper

- (NSMutableArray *)trainingProgramCalculation{
    InitialCapabilities *capabilities = [CoreDataHelper loadInitialCapabilities][0];
    if ([capabilities isKindOfClass:[CurrentCapabilities class]]){
        capabilities = [CoreDataHelper loadInitialCapabilities][1];
        self.initialCapabilities = capabilities;
    }else {
        self.initialCapabilities = capabilities;
    }
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSMutableArray <NSMutableArray *> *preparedExercises = [NSMutableArray new];
    Exercise *benchPress;
    Exercise *deadlift;
    Exercise *squad;
    Exercise *militaryPress;
    NSSet <Exercise*> *auxExerciseSet =self.initialCapabilities.initialBestExercises;
    for(Exercise *exercise in auxExerciseSet){
        if([exercise.type isEqualToString:gBenchPress]){
            benchPress = exercise;
        }else if ([exercise.type isEqualToString:gDeadlift]) {
            deadlift = exercise;
        }else if ([exercise.type isEqualToString:gSquad ]){
            squad = exercise;
        }else if ([exercise.type isEqualToString:gMilitaryPress]){
            militaryPress = exercise;
        }
    }
    
    NSMutableArray *arrayOfExercises1 = [NSMutableArray new];
    Exercise *firstExercise = [CoreDataHelper createExerciseInContext:context :gMaximumReps :0.9*benchPress.weight :gBenchPress];
    Exercise *secondExercise = [CoreDataHelper createExerciseInContext:context :10 :12 :gBicepsHammer];
    Exercise *thirdExercise = [CoreDataHelper createExerciseInContext:context :12 :45 :gPullDown];
    [arrayOfExercises1 addObject:firstExercise];
    [arrayOfExercises1 addObject:secondExercise];
    [arrayOfExercises1 addObject:thirdExercise];
    auxExerciseSet = [NSSet setWithArray:arrayOfExercises1];
    [preparedExercises addObject:arrayOfExercises1];
    [self saveDataToFireBase:@"Day1" :arrayOfExercises1];
    TrainingDay *trainingDay1 = [CoreDataHelper createTrainingDayInContext:context :auxExerciseSet :@"Day1"];
    
    NSMutableArray *arrayOfExercises2 = [NSMutableArray new];
    firstExercise = [CoreDataHelper createExerciseInContext:context :gMaximumReps :0.9*militaryPress.weight :gMilitaryPress];
    secondExercise = [CoreDataHelper createExerciseInContext:context :gMaximumReps :0.9*squad.weight :gSquad];
    thirdExercise = [CoreDataHelper createExerciseInContext:context :gMaximumReps :100 :gLegPress];
    [arrayOfExercises2 addObject:firstExercise];
    [arrayOfExercises2 addObject:secondExercise];
    [arrayOfExercises2 addObject:thirdExercise];
    auxExerciseSet = [NSSet setWithArray:arrayOfExercises2];
    [preparedExercises addObject:arrayOfExercises2];
    [self saveDataToFireBase:@"Day2" :arrayOfExercises2];
    TrainingDay *trainingDay2 = [CoreDataHelper createTrainingDayInContext:context :auxExerciseSet :@"Day2"];
    
    
    NSMutableArray *arrayOfExercises3 = [NSMutableArray new];
    firstExercise = [CoreDataHelper createExerciseInContext:context :gMaximumReps :0.9*deadlift.weight :gDeadlift];
    secondExercise = [CoreDataHelper createExerciseInContext:context :gMaximumReps :0.8*benchPress.weight :gBenchPressNarrow];
    thirdExercise = [CoreDataHelper createExerciseInContext:context :gMaximumReps :50 :gPullDown];
    [arrayOfExercises3 addObject:firstExercise];
    [arrayOfExercises3 addObject:secondExercise];
    [arrayOfExercises3 addObject:thirdExercise];
    auxExerciseSet = [NSSet setWithArray:arrayOfExercises3];
    [preparedExercises addObject:arrayOfExercises3];
    [self saveDataToFireBase:@"Day3" :arrayOfExercises3];
    TrainingDay *trainingDay3 = [CoreDataHelper createTrainingDayInContext:context :auxExerciseSet :@"Day3"];
    
    arrayOfExercises1 = [NSMutableArray new];
    firstExercise = [CoreDataHelper createExerciseInContext:context :gIntermidiateReps :benchPress.weight :gBenchPress];
    secondExercise = [CoreDataHelper createExerciseInContext:context :gMaximumReps :12 :gBicepsHammer];
    thirdExercise = [CoreDataHelper createExerciseInContext:context :12 :60 :gPullDown];
    [arrayOfExercises1 addObject:firstExercise];
    [arrayOfExercises1 addObject:secondExercise];
    [arrayOfExercises1 addObject:thirdExercise];
    auxExerciseSet = [NSSet setWithArray:arrayOfExercises1];
    [preparedExercises addObject:arrayOfExercises1];
    [self saveDataToFireBase:@"Day4" :arrayOfExercises1];
    TrainingDay *trainingDay4 = [CoreDataHelper createTrainingDayInContext:context :auxExerciseSet :@"Day4"];
    
    arrayOfExercises2 = [NSMutableArray new];
    firstExercise = [CoreDataHelper createExerciseInContext:context :gIntermidiateReps :militaryPress.weight :gMilitaryPress];
    secondExercise = [CoreDataHelper createExerciseInContext:context :gIntermidiateReps :squad.weight :gSquad];
    thirdExercise = [CoreDataHelper createExerciseInContext:context :gMaximumReps :100 :gLegPress];
    [arrayOfExercises2 addObject:firstExercise];
    [arrayOfExercises2 addObject:secondExercise];
    [arrayOfExercises2 addObject:thirdExercise];
    auxExerciseSet = [NSSet setWithArray:arrayOfExercises2];
    [preparedExercises addObject:arrayOfExercises2];
    [self saveDataToFireBase:@"Day5" :arrayOfExercises2];
    TrainingDay *trainingDay5 = [CoreDataHelper createTrainingDayInContext:context :auxExerciseSet :@"Day5"];
    
    
    arrayOfExercises3 = [NSMutableArray new];
    firstExercise = [CoreDataHelper createExerciseInContext:context :gIntermidiateReps :deadlift.weight :gDeadlift];
    secondExercise = [CoreDataHelper createExerciseInContext:context :gIntermidiateReps :benchPress.weight :gBenchPressNarrow];
    thirdExercise = [CoreDataHelper createExerciseInContext:context :10 :50 :gPullDown];
    [arrayOfExercises3 addObject:firstExercise];
    [arrayOfExercises3 addObject:secondExercise];
    [arrayOfExercises3 addObject:thirdExercise];
    auxExerciseSet = [NSSet setWithArray:arrayOfExercises3];
    [preparedExercises addObject:arrayOfExercises3];
    [self saveDataToFireBase:@"Day6" :arrayOfExercises3];
    TrainingDay *trainingDay6 = [CoreDataHelper createTrainingDayInContext:context :auxExerciseSet :@"Day6"];
    
    arrayOfExercises1 = [NSMutableArray new];
    firstExercise = [CoreDataHelper createExerciseInContext:context :glowReps :benchPress.weight :gBenchPress];
    secondExercise = [CoreDataHelper createExerciseInContext:context :gMaximumReps :14 :gBicepsHammer];
    thirdExercise = [CoreDataHelper createExerciseInContext:context :12 :60 :gPullDown];
    [arrayOfExercises1 addObject:firstExercise];
    [arrayOfExercises1 addObject:secondExercise];
    [arrayOfExercises1 addObject:thirdExercise];
    auxExerciseSet = [NSSet setWithArray:arrayOfExercises1];
    [preparedExercises addObject:arrayOfExercises1];
    [self saveDataToFireBase:@"Day4" :arrayOfExercises1];
    TrainingDay *trainingDay7 = [CoreDataHelper createTrainingDayInContext:context :auxExerciseSet :@"Day7"];
    
    arrayOfExercises2 = [NSMutableArray new];
    firstExercise = [CoreDataHelper createExerciseInContext:context :glowReps :militaryPress.weight :gMilitaryPress];
    secondExercise = [CoreDataHelper createExerciseInContext:context :glowReps :squad.weight :gSquad];
    thirdExercise = [CoreDataHelper createExerciseInContext:context :gMaximumReps :100 :gLegPress];
    [arrayOfExercises2 addObject:firstExercise];
    [arrayOfExercises2 addObject:secondExercise];
    [arrayOfExercises2 addObject:thirdExercise];
    auxExerciseSet = [NSSet setWithArray:arrayOfExercises2];
    [preparedExercises addObject:arrayOfExercises2];
    [self saveDataToFireBase:@"Day5" :arrayOfExercises2];
    TrainingDay *trainingDay8 = [CoreDataHelper createTrainingDayInContext:context :auxExerciseSet :@"Day8"];
    
    
    arrayOfExercises3 = [NSMutableArray new];
    firstExercise = [CoreDataHelper createExerciseInContext:context :glowReps :deadlift.weight :gDeadlift];
    secondExercise = [CoreDataHelper createExerciseInContext:context :glowReps :benchPress.weight :gBenchPressNarrow];
    thirdExercise = [CoreDataHelper createExerciseInContext:context :gMaximumReps :60 :gPullDown];
    [arrayOfExercises3 addObject:firstExercise];
    [arrayOfExercises3 addObject:secondExercise];
    [arrayOfExercises3 addObject:thirdExercise];
    auxExerciseSet = [NSSet setWithArray:arrayOfExercises3];
    [preparedExercises addObject:arrayOfExercises3];
    [self saveDataToFireBase:@"Day6" :arrayOfExercises3];
    TrainingDay *trainingDay9 = [CoreDataHelper createTrainingDayInContext:context :auxExerciseSet :@"Day9"];
    
    [CoreDataHelper createCurrentProgramInContext:context :[NSSet setWithArray:@[trainingDay1, trainingDay2, trainingDay3, trainingDay4,trainingDay5, trainingDay6, trainingDay7, trainingDay8,trainingDay9]]];
    [appDelegate saveContext];
    return preparedExercises;
}


- (NSMutableArray *)prepareExistenceProgram:(NSArray <CurrentProgram*>*)program{
    CurrentProgram *currentProgram = program[0];
    NSMutableArray<NSMutableArray *> *preparedExercises = [NSMutableArray new];
    for (int i=0;i<3;i++){
        [preparedExercises addObject:[@[] mutableCopy]];
    }
    
    for (TrainingDay *trainingDay in currentProgram.trainingDays) {
        switch ([trainingDay.dateOfExecution characterAtIndex:3]) {
            case '1':
                preparedExercises[0] =  [self fillExerciseDay:trainingDay];
                break;
            case '2':
                preparedExercises[1] =  [self fillExerciseDay:trainingDay];
                break;
            case '3':
                preparedExercises[2] =  [self fillExerciseDay:trainingDay];
                break;
            case '4':
                preparedExercises[3] =  [self fillExerciseDay:trainingDay];
                break;
            default:
                break;
        }
    }
    return preparedExercises;

}

- (NSMutableArray *)fillExerciseDay:(TrainingDay *)trainingDay{
    NSMutableArray *preparedDay = [NSMutableArray new];
    for (Exercise *exercise in trainingDay.trainingDayExercises){
        [preparedDay addObject:exercise];
    }
    
    return preparedDay;
}

- (void)saveDataToFireBase :(NSString*)day :(NSMutableArray*)exercises{
    [[FIRAuth auth]
     signInAnonymouslyWithCompletion:^(FIRUser *_Nullable user, NSError *_Nullable error) {
         if (!error) {
             self.uid = user.uid;
             _ref=[[FIRDatabase database]reference];
             [self writeProgramPost: day:exercises];
         }else {
             
         }
     }];
}

- (void)writeProgramPost:(NSString*)day :(NSMutableArray*)exercises{
    NSString *key = self.uid;
    NSMutableDictionary <NSString*, NSDictionary*>* dictionaryExercises = [NSMutableDictionary new];
    for (Exercise *exercise in exercises) {
        [dictionaryExercises setValue:@{[[NSNumber numberWithInt: exercise.weight ] stringValue] :[NSNumber numberWithInt:exercise.reps]} forKey:exercise.type];
    }
    
    NSDictionary *post = [NSDictionary dictionaryWithDictionary:dictionaryExercises] ;
    
    NSDictionary *childUpdates = @{[NSString stringWithFormat:@"/user/%@/%@/%@", key,@"Current Program",day]: post};
                           
    [_ref updateChildValues:childUpdates];
}


@end
