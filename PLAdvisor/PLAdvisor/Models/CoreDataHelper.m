//
//  coreDataHelper.m
//  PLAdvisor
//
//  Created by Admin on 08.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "CoreDataHelper.h"


@implementation CoreDataHelper

#pragma mark - Entity Creation
+ (Exercise *) createExerciseInContext : (NSManagedObjectContext *)context : (int)reps : (int)weight : (NSString *)type {
    NSEntityDescription *exerciseDescr = [NSEntityDescription entityForName:@"Exercise" inManagedObjectContext:context];
    Exercise *exercise = [[Exercise alloc] initWithEntity:exerciseDescr insertIntoManagedObjectContext:context];
    exercise.reps = reps;
    exercise.weight = weight;
    exercise.type = type;
    
    return exercise;
}

+ (TrainingDay *)createTrainingDayInContext : (NSManagedObjectContext *)context : (NSSet <Exercise*>*) exercises :(NSString *) date{
    NSEntityDescription *dayDescr = [NSEntityDescription entityForName:@"TrainingDay" inManagedObjectContext:context];
    TrainingDay *trainingDay = [[TrainingDay alloc] initWithEntity:dayDescr insertIntoManagedObjectContext:context];
    trainingDay.trainingDayExercises = exercises;
    trainingDay.dateOfExecution = date;
    
    return trainingDay;
}

+ (CurrentProgram *)createCurrentProgramInContext : (NSManagedObjectContext *)context : (NSSet <TrainingDay*>*) trainingDays{
    NSEntityDescription *programDescr = [NSEntityDescription entityForName:@"CurrentProgram" inManagedObjectContext:context];
    CurrentProgram *currentProgram = [[CurrentProgram alloc] initWithEntity:programDescr insertIntoManagedObjectContext:context];
    currentProgram.trainingDays = trainingDays;
    return currentProgram;
}

+ (Injury *)createInjuryInContext:(NSManagedObjectContext *)context : (NSString *)injuryType : (NSString *)problemDate{
    NSEntityDescription *injuryDescr = [NSEntityDescription entityForName:@"Injury" inManagedObjectContext:context];
    Injury *injury = [[Injury alloc] initWithEntity:injuryDescr insertIntoManagedObjectContext:context];
    injury.problemDate = problemDate;
    injury.injuryType = injuryType;
    
    return injury;
}

+ (CurrentCapabilities *)createCurrentCapabilitiesInContext: (NSManagedObjectContext *)context : (NSSet <Exercise *>*) exercises : (NSSet <Injury *>*) injuries : (int) ownWeight : (int) ownHeight{
    NSEntityDescription *currentCapabilitiesDescr = [NSEntityDescription entityForName:@"CurrentCapabilities" inManagedObjectContext:context];
    CurrentCapabilities *currentCapabilities = [[CurrentCapabilities alloc] initWithEntity:currentCapabilitiesDescr insertIntoManagedObjectContext:context];
    currentCapabilities.currentBestExercises = exercises;
    currentCapabilities.injuriesList = injuries;
    currentCapabilities.ownWeight = ownWeight;
    currentCapabilities.ownHeight = ownHeight;
    
    return currentCapabilities;
}

+ (InitialCapabilities *)createInitialCapabilitiesInContext: (NSManagedObjectContext *)context :(NSSet <Exercise *>*)exercises :(int) ownWeight : (int) ownHeight{
    NSEntityDescription *initialCapabilitiesDescr = [NSEntityDescription entityForName:@"InitialCapabilities" inManagedObjectContext:context];
    InitialCapabilities *initialCapabilities = [[InitialCapabilities alloc] initWithEntity:initialCapabilitiesDescr insertIntoManagedObjectContext:context];
    initialCapabilities.initialBestExercises = exercises;
    initialCapabilities.ownHeight = ownHeight;
    initialCapabilities.ownWeight = ownWeight;
    
    return initialCapabilities;
}

#pragma mark - Entity Loading
+(NSArray <CurrentProgram *>*)loadCurrentProgram{
    NSFetchRequest *fetchRequest = [CurrentProgram fetchRequest];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSArray <CurrentProgram *> *program = [appDelegate.persistentContainer.viewContext executeFetchRequest:fetchRequest error:nil];
    
    return program;
}

+ (NSArray <TrainingDay *> *)loadTrainingDays{
    NSFetchRequest *fetchRequest = [TrainingDay fetchRequest];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray <TrainingDay *> * trainingDays = [appDelegate.persistentContainer.viewContext
                                              executeFetchRequest:
                                              fetchRequest error:nil];
    return  trainingDays;
}

+ (NSArray <InitialCapabilities*> *) loadInitialCapabilities{
    NSFetchRequest *fetchReques = [InitialCapabilities fetchRequest];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSArray <InitialCapabilities*> *initialCapabilities = [appDelegate.persistentContainer.viewContext
                                executeFetchRequest:fetchReques
                                error:nil];
    return initialCapabilities;
}

+ (NSArray <CurrentCapabilities*> *)loadCurrentCapabilities{
    NSFetchRequest *fetchRequest = [CurrentCapabilities fetchRequest];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSArray <CurrentCapabilities*> *currentCapabilities = [appDelegate.persistentContainer.viewContext
                                                           executeFetchRequest:fetchRequest
                                                           error:nil];
    return currentCapabilities;
}

+ (NSArray <Injury*> *)loadInjuries: (AppDelegate *)appDelegate{
    NSFetchRequest *fetchRequest = [CurrentCapabilities fetchRequest];
    NSArray <Injury*>*injuries =[appDelegate.persistentContainer.viewContext
                                executeFetchRequest:fetchRequest
                                error:nil];
    
    return injuries;
}

+ (NSArray <Exercise*> *) loadExercises{
    NSFetchRequest *fetchReques = [Exercise fetchRequest];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray <Exercise*> *exercises = [appDelegate.persistentContainer.viewContext executeFetchRequest:fetchReques error:nil];
    return exercises;
}

#pragma mark - Auxiliary Methods For Entities
+ (void)clearChosenEntity:(NSString *)entityName{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSFetchRequest *allEntities = [[NSFetchRequest alloc] init];
    [allEntities setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    [allEntities setIncludesPropertyValues:NO];
    NSError *error = nil;
    NSArray *entities = [context executeFetchRequest:allEntities error:&error];
    for (NSManagedObject *entity in entities){
        [context deleteObject:entity];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

+ (void) redefineExercise :(Exercise *)exercise : (int)reps : (int)weight : (NSString *)type{
    exercise.reps = reps;
    exercise.weight = weight;
    exercise.type = type;
}

+ (NSMutableDictionary<NSString*, NSNumber*>*)setOfExercisesToDictionary:(NSSet<Exercise*>*)exercises{
    NSMutableDictionary <NSString*, NSNumber*>* capabilitiesDictionary= [NSMutableDictionary new];
    NSSet <Exercise*>* exercisesSet = exercises;
    NSLog(@"%lu", (unsigned long)exercises.count);
    for (Exercise *exercise in exercisesSet) {
        [capabilitiesDictionary setValue:[NSNumber numberWithInt: exercise.weight ] forKey:exercise.type];
        NSLog(@"weight:%d type:%@ reps:%d",exercise.weight , exercise.type , exercise.reps );
    }
    return capabilitiesDictionary;
}
@end
