//
//  TrainingDayHelper.h
//  PLAdvisor
//
//  Created by Admin on 18.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrainingDay+CoreDataClass.h"
#import "Exercise+CoreDataClass.h"

@interface TrainingDayHelper : NSObject

@property (nonatomic,copy) NSArray <TrainingDay*>* trainingDaysFromBase;
@property (nonatomic, strong) NSMutableArray <TrainingDay*>* preparedForDisplay;

- (NSMutableDictionary<NSString*,Exercise*>*) prepareExercieseForPlot;

@end
