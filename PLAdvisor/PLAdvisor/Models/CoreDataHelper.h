//
//  coreDataHelper.h
//  PLAdvisor
//
//  Created by Admin on 08.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Exercise+CoreDataClass.h"
#import "Exercise+CoreDataProperties.h"
#import "InitialCapabilities+CoreDataClass.h"
#import "InitialCapabilities+CoreDataProperties.h"
#import "CurrentProgram+CoreDataClass.h"
#import "CurrentProgram+CoreDataProperties.h"
#import "CurrentCapabilities+CoreDataClass.h"
#import "CurrentCapabilities+CoreDataProperties.h"
#import "TrainingDay+CoreDataClass.h"
#import "TrainingDay+CoreDataProperties.h"
#import "Injury+CoreDataClass.h"
#import "Injury+CoreDataProperties.h"
#import "AppDelegate.h"

@interface CoreDataHelper : NSObject

@property (nonatomic, strong) CurrentCapabilities *currentCapabilities;
@property (nonatomic, strong) InitialCapabilities *initialCapabilities;


+ (NSArray <InitialCapabilities*> *) loadInitialCapabilities;
+ (NSArray <TrainingDay *> *)loadTrainingDays;
+ (NSArray <Exercise*> *) loadExercises;
+ (NSArray <CurrentCapabilities*> *)loadCurrentCapabilities;
+ (NSArray <Injury*> *)loadInjuries:(AppDelegate *)appDelegate;
+ (NSArray <CurrentProgram *>*)loadCurrentProgram;

+ (void)clearChosenEntity:(NSString *)entityName;
+ (void) redefineExercise :(Exercise *)exercise : (int)reps : (int)weight : (NSString *)type;

+ (Exercise *) createExerciseInContext: (NSManagedObjectContext *)context : (int)reps : (int)weight : (NSString *)type;
+ (TrainingDay *)createTrainingDayInContext: (NSManagedObjectContext *)context : (NSSet <Exercise*>*) exercises :(NSString *) date;
+ (CurrentProgram *)createCurrentProgramInContext: (NSManagedObjectContext *)context : (NSSet <TrainingDay*>*) trainingDays;
+ (InitialCapabilities *)createInitialCapabilitiesInContext: (NSManagedObjectContext *)context :(NSSet <Exercise *>*)exercises :(int) ownWeight : (int) ownHeight;
+ (Injury *)createInjuryInContext:(NSManagedObjectContext *)context : (NSString *)injuryType : (NSString *)problemDate;
+ (CurrentCapabilities *)createCurrentCapabilitiesInContext: (NSManagedObjectContext *)context : (NSSet <Exercise *>*) exercises : (NSSet <Injury *>*) injuries : (int) ownWeight : (int) ownHeight;

+ (NSMutableDictionary<NSString*, NSNumber*>*)setOfExercisesToDictionary:(NSSet*)exercises;
@end
