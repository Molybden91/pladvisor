//
//  CapabilitiesHelper.m
//  PLAdvisor
//
//  Created by Admin on 18.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "CapabilitiesHelper.h"
#import "CoreDataHelper.h"


@implementation CapabilitiesHelper

- (CurrentCapabilities *)currentCapabilities{
    if (_currentCapabilities==nil){
        _currentCapabilities = [CoreDataHelper loadCurrentCapabilities][0];
    }
    return _currentCapabilities;
}

- (InitialCapabilities *)initialCapabilities{
    if (_initialCapabilities==nil){
        _initialCapabilities = [CoreDataHelper loadInitialCapabilities][0];
    }
    return  _initialCapabilities;
}

- (void)redefineCurrentCapabilities: (NSManagedObjectContext *)context : (NSSet <Exercise *>*) exercises : (NSSet <Injury *>*) injuries : (int) ownWeight : (int) ownHeight{
    [CoreDataHelper clearChosenEntity:@"CurrentCapabilities"];
    self.currentCapabilities = [CoreDataHelper createCurrentCapabilitiesInContext:context :exercises :injuries :ownWeight :ownHeight];
}

@end
