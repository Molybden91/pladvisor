//
//  ProgramCreationHelper.h
//  PLAdvisor
//
//  Created by Admin on 19.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InitialCapabilities+CoreDataClass.h"
#import "CurrentProgram+CoreDataClass.h"

@interface ProgramCreationHelper : NSObject

@property (nonatomic, strong) InitialCapabilities *initialCapabilities;

- (NSMutableArray *)trainingProgramCalculation;

- (NSMutableArray *)prepareExistenceProgram:(NSArray <CurrentProgram*>*)program;
@end
