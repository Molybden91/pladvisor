//
//  Constants.h
//  PLAdvisor
//
//  Created by Admin on 09.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const gBenchPress;
FOUNDATION_EXPORT NSString *const gBenchPressNarrow;
FOUNDATION_EXPORT NSString *const gDeadlift;
FOUNDATION_EXPORT NSString *const gSquad;
FOUNDATION_EXPORT NSString *const gMilitaryPress;
FOUNDATION_EXPORT NSString *const gBicepsHammer;
FOUNDATION_EXPORT NSString *const gLegPress;
FOUNDATION_EXPORT NSString *const gOwnWeight;
FOUNDATION_EXPORT NSString *const gOwnHeight;
FOUNDATION_EXPORT NSString *const gPullDown;
FOUNDATION_EXPORT NSString *const gRodThrustInclination;
FOUNDATION_EXPORT NSString *const gBenchPressWithBar;


FOUNDATION_EXPORT int const gMaximumReps;
FOUNDATION_EXPORT int const gPreMaximumReps;
FOUNDATION_EXPORT int const gIntermidiateReps;
FOUNDATION_EXPORT int const glowReps;
FOUNDATION_EXPORT int const gOneRep;
