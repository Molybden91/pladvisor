//
//  Constants.m
//  PLAdvisor
//
//  Created by Admin on 09.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "Constants.h"

NSString *const gBenchPress = @"BenchPress";
NSString *const gBenchPressNarrow = @"BenchPressNarrow";
NSString *const gDeadlift = @"Deadlift";
NSString *const gSquad = @"Squat";
NSString *const gMilitaryPress = @"MilitaryPress";
NSString *const gBicepsHammer = @"BicepsHammer";
NSString *const gLegPress = @"LegPress";
NSString *const gOwnWeight = @"OwnWeight";
NSString *const gOwnHeight = @"OwnHeight";
NSString *const gPullDown = @"PullDown";
NSString *const gRodThrustInclination = @"RodThrustInclination";
NSString *const gBenchPressWithBar = @"BenchPressWithBar";


int const gMaximumReps = 10;
int const gPreMaximumReps = 8;
int const gIntermidiateReps = 6;
int const glowReps = 3;
int const gOneRep = 1;
