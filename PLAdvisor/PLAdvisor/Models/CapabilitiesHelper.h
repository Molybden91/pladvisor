//
//  CapabilitiesHelper.h
//  PLAdvisor
//
//  Created by Admin on 18.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CurrentCapabilities+CoreDataClass.h"
#import "InitialCapabilities+CoreDataClass.h"

@interface CapabilitiesHelper : NSObject

@property (nonatomic, strong) CurrentCapabilities *currentCapabilities;
@property (nonatomic, strong) InitialCapabilities *initialCapabilities;


- (void)redefineCurrentCapabilities: (NSManagedObjectContext *)context : (NSSet <Exercise *>*) exercises : (NSSet <Injury *>*) injuries : (int) ownWeight : (int) ownHeight;

@end
