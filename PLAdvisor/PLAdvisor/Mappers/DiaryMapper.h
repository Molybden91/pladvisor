//
//  DiaryMapper.h
//  PLAdvisor
//
//  Created by Admin on 04.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiaryMapper : NSObject

+ (void)modelFromJson:(NSDictionary *)initialJson;

@end
