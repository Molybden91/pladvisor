//
//  DiaryMapper.m
//  PLAdvisor
//
//  Created by Admin on 04.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DiaryMapper.h"
#import "CoreDataHelper.h"
#import "Exercise+CoreDataClass.h"
#import "TrainingDay+CoreDataClass.h"

@implementation DiaryMapper

+ (void)modelFromJson:(NSDictionary *)initialJson{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSDictionary *json = [initialJson  valueForKey:@"Diary"];
    
    for (NSString *key in json.allKeys) {
        
        NSMutableSet *exercisesSet = [NSMutableSet new];
        NSDictionary <NSString*, NSDictionary*> *auxDictionary = [json valueForKey: key];
        for (NSString* keyType in auxDictionary.allKeys) {
            
            int reps =[[auxDictionary valueForKey:keyType].allKeys [0] intValue];
            int weight = [[auxDictionary valueForKey:keyType].allValues[0] intValue];
            
            [exercisesSet addObject:[CoreDataHelper createExerciseInContext:context :reps :weight : keyType]];
        }
        [CoreDataHelper createTrainingDayInContext:context :exercisesSet :key];
    }
}

@end
