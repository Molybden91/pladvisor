//
//  CurrentCapabilitiesMapper.m
//  PLAdvisor
//
//  Created by Admin on 04.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "CapabilitiesMapper.h"
#import "CoreDataHelper.h"
#import "Constants.h"


@implementation CapabilitiesMapper

+ (void)modelFromJson:(NSDictionary *)initialJson :(NSString*)capabilitiesIdentifier{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSString *identifier = capabilitiesIdentifier;
    NSDictionary *json = [initialJson  valueForKey:identifier];
    
    Exercise *benchpress = [CoreDataHelper createExerciseInContext:context :gOneRep :[json[gBenchPress] intValue] :gBenchPress];
    Exercise *deadlift = [CoreDataHelper createExerciseInContext:context :gOneRep :[json[gDeadlift] intValue] :gDeadlift];
    Exercise *squat = [CoreDataHelper createExerciseInContext:context :gOneRep :[json[gSquad] intValue] :gSquad];
    Exercise *militaryPress = [CoreDataHelper createExerciseInContext:context :gOneRep :[json[gMilitaryPress] intValue] :gMilitaryPress];
    NSLog(@"weights:%@%@%@%@",json[gBenchPress],json[gDeadlift], json[gSquad], json[gMilitaryPress]);
    NSSet *exercisesSet = [NSSet setWithObjects:benchpress,deadlift,squat,militaryPress, nil ];
    
    NSMutableSet <Injury*>* injuries = [NSMutableSet new];
    NSDictionary *injuriesDictionary = json[@"Injuries"];
    for (NSString* key in injuriesDictionary) {
        Injury *injury = [CoreDataHelper createInjuryInContext:context :key :injuriesDictionary[key]];
        [injuries addObject:injury];
    }
    
    if ([capabilitiesIdentifier isEqualToString:@"Current Capabilities"]){
        [CoreDataHelper createCurrentCapabilitiesInContext:context :exercisesSet :injuries :[json[gOwnWeight] intValue]  :[json[gOwnHeight] intValue]];
    } else if([capabilitiesIdentifier isEqualToString:@"Initial Capabilities"]){
        [CoreDataHelper createInitialCapabilitiesInContext:context :exercisesSet :[json[gOwnWeight] intValue] :[json[gOwnHeight] intValue]];
    }
}

@end
