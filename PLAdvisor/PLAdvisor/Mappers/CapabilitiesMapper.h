//
//  CurrentCapabilitiesMapper.h
//  PLAdvisor
//
//  Created by Admin on 04.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CurrentCapabilities+CoreDataClass.h"
#import "Exercise+CoreDataClass.h"

@interface CapabilitiesMapper : NSObject

+ (void)modelFromJson:(NSDictionary *)initialJson :(NSString*)capabilitiesIdentifier;

@end
