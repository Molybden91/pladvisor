//
//  AnalyzeDataViewController.m
//  PLAdvisor
//
//  Created by Admin on 07.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AnalyzeDataViewController.h"
#import "TrainingDayViewingViewController.h"
#import "RightNavigationButton.h"
#import "AnalyzeDataTableViewCell.h"

#import "TrainingDayHelper.h"
#import "TrainingDay+CoreDataClass.h"

static  NSString* const kCellID= @"AnalyzeDataID";

@interface AnalyzeDataViewController () <UITableViewDataSource, AnalyzedataTableViewCellDelegate>

@property (nonatomic, strong) NSArray <TrainingDay *>* trainingDays;
@property (nonatomic, strong) TrainingDay *currentTrainingDay;

@end

@implementation AnalyzeDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadTrainingDays];
    [self configureInitialDropDownConditions];
    [self configureRightNavigationButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Configuration
- (void)loadTrainingDays{
    TrainingDayHelper *trainingDayHelper = [TrainingDayHelper new];
    self.trainingDays = trainingDayHelper.preparedForDisplay;
}

- (void)configureRightNavigationButton{
    RightNavigationButton *doneButton = [RightNavigationButton new];
    doneButton = (RightNavigationButton *)[doneButton configureButton:@"DONE"];
    [doneButton addTarget:self action:@selector(didTapDoneButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:doneButton];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.trainingDays.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AnalyzeDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID forIndexPath:indexPath];
    cell.delegate = self;
    [cell configureCell:self.trainingDays[indexPath.row]];
    return cell;
}

#pragma mark - Transition Actions
- (IBAction)didTapDoneButton:(id)sender{
    [self performSegueWithIdentifier:@"toTrainingAnalyze" sender:self];
}

- (void)didTapTrainingDayButton:(AnalyzeDataTableViewCell *)cell{
    self.currentTrainingDay = cell.insertTrainingDay;
    [self performSegueWithIdentifier:@"toTrainingDayTransition" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"toTrainingDayTransition"]){
        TrainingDayViewingViewController *trainingViewController = segue.destinationViewController;
        trainingViewController.trainingDay =self.currentTrainingDay;
    }else if ([segue.identifier isEqualToString:@"toTrainingAnalyze"]){
        
    }
}

#pragma mark - DropDown Conditions
- (void)configureInitialDropDownConditions{
    [self.monthSelectionButton setEnabled:NO];
    [self.yearSelectionButton setEnabled:NO];
    [self.allTimeselectionButton setEnabled:NO];
    
    [self.monthSelectionButton setBackgroundColor:[UIColor whiteColor]];
    [self.monthSelectionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.yearSelectionButton setBackgroundColor:[UIColor whiteColor]];
    [self.yearSelectionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.allTimeselectionButton setBackgroundColor:[UIColor whiteColor]];
    [self.allTimeselectionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (void)dropDownConditions: (BOOL)isEnabled{
    [self.monthSelectionButton setEnabled:isEnabled];
    [self.yearSelectionButton setEnabled:isEnabled];
    [self.allTimeselectionButton setEnabled:isEnabled];
    
    if (isEnabled){
        [self.monthSelectionButton setBackgroundColor:[UIColor blackColor]];
        [self.monthSelectionButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        [self.yearSelectionButton setBackgroundColor:[UIColor grayColor]];
        [self.yearSelectionButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        [self.allTimeselectionButton setBackgroundColor:[UIColor lightGrayColor]];
        [self.allTimeselectionButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
    }else {
        [self.monthSelectionButton setBackgroundColor:[UIColor whiteColor]];
        [self.monthSelectionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.yearSelectionButton setBackgroundColor:[UIColor whiteColor]];
        [self.yearSelectionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.allTimeselectionButton setBackgroundColor:[UIColor whiteColor]];
        [self.allTimeselectionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

- (void)actionWithEachSelect: (NSString *)textForLabel{
    [self dropDownConditions:NO];
    self.dropDownLabel.text = textForLabel;
}

#pragma mark - Selecting of DropDown
- (IBAction)didTapDropDown:(id)sender {
    if (self.monthSelectionButton.isEnabled ==NO){
        [self dropDownConditions:YES];
    }else {
        [self dropDownConditions:NO];
    }
}

- (IBAction)didTapMonth:(id)sender {
    [self actionWithEachSelect:self.monthSelectionButton.titleLabel.text];
    self.dropDownButton.titleLabel.text = @"";
}

- (IBAction)didTapYear:(id)sender {
    [self actionWithEachSelect:self.yearSelectionButton.titleLabel.text];
    self.dropDownButton.titleLabel.text = @"";
}

- (IBAction)didTapAll:(id)sender {
    [self actionWithEachSelect:self.allTimeselectionButton.titleLabel.text];
    self.dropDownButton.titleLabel.text = @"";
}

@end
