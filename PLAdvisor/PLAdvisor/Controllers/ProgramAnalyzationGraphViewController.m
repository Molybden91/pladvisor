//
//  ProgramAnalyzationGraphViewController.m
//  PLAdvisor
//
//  Created by Admin on 07.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ProgramAnalyzationGraphViewController.h"
#import "ProgramAnalyzationGraphCanvas.h"

@interface ProgramAnalyzationGraphViewController ()

@property (nonatomic, strong) ProgramAnalyzationGraphCanvas *canvas;

@end

@implementation ProgramAnalyzationGraphViewController

- (instancetype)init{
    return [super init];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    return [super initWithCoder:aDecoder];
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    return [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
}

- (void)loadView {
    [super loadView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureUI{
    self.view.backgroundColor = [UIColor blackColor];
    self.canvas = [ProgramAnalyzationGraphCanvas new];
    [self.view  addSubview:self.canvas];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    CGFloat horizontalOffset = 20;
    CGFloat verticalOffset = 50;
    
    CGFloat width = self.view.bounds.size.width - 2* horizontalOffset;
    CGFloat height = self.view.bounds.size.height - 2 *verticalOffset;
    
    self.canvas.frame= CGRectMake(horizontalOffset, verticalOffset, width, height);
}

@end
