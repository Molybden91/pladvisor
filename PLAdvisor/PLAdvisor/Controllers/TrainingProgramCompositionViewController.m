//
//  TrainingProgramCompositionViewController.m
//  PLAdvisor
//
//  Created by Admin on 06.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//
#import "CreatedTrainingProgramViewController.h"
#import "TrainingProgramCompositionViewController.h"
#import "RightNavigationButton.h"
#import "CentralNavigationButton.h"

#import "AppDelegate.h"
#import "InitialCapabilities+CoreDataClass.h"
#import "Exercise+CoreDataClass.h"
#import "CoreDataHelper.h"
#import "Constants.h"

#import <Firebase/Firebase.h>
#import <FirebaseDatabase/FirebaseDatabase.h>

static NSInteger const kNumberOfRepsForCapabilityDetermination = 10;

@interface TrainingProgramCompositionViewController () <UITextFieldDelegate>

@property (nonatomic, strong) NSMutableDictionary  *storedData;
@property (nonatomic, strong) FIRDatabaseReference *ref;
@property (nonatomic, strong) InitialCapabilities *initialCapabilities;
@property NSString *uid;

//titles of entering data for future training program for our athlet
@property (weak, nonatomic) IBOutlet UITextField *titleOwnHeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *titleOwnWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *titleBenchPressTextField;
@property (weak, nonatomic) IBOutlet UITextField *titileDeadliftTextField;
@property (weak, nonatomic) IBOutlet UITextField *titleSqudTextField;
@property (weak, nonatomic) IBOutlet UITextField *titleMilitaryPressTextField;
@property (weak, nonatomic) IBOutlet UITextField *titleStrengthCapabilities;

//Text Fields for calculation of future training program for our athlet
@property (weak, nonatomic) IBOutlet UITextField *ownHeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *ownWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *benchPressTextField;
@property (weak, nonatomic) IBOutlet UITextField *deadliftTextField;
@property (weak, nonatomic) IBOutlet UITextField *squadTextField;
@property (weak, nonatomic) IBOutlet UITextField *militaryPressTextField;

@end

@implementation TrainingProgramCompositionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureRightNavigationButton];
    [self configureTitleNavigationButton];
    [self configureTextFields];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UI Configuration
- (void)configureRightNavigationButton{
    RightNavigationButton *doneButton = [RightNavigationButton new];
    doneButton = (RightNavigationButton *)[doneButton configureButton:@"DONE"];
    [doneButton addTarget:self action:@selector(didTapDoneButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:doneButton];
}

- (void)configureTitleNavigationButton{
    CentralNavigationButton *titleButton = [CentralNavigationButton new];
    titleButton = (CentralNavigationButton *)[titleButton configureButton:@"Reset Initial Data"];
    [titleButton addTarget:self action:@selector(didTapReset:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleButton;
}

-(void)configureTextFields{
    self.storedData =  [NSMutableDictionary new];
    self.ownHeightTextField.delegate = self;
    self.ownWeightTextField.delegate = self;
    self.benchPressTextField.delegate = self;
    self.deadliftTextField.delegate = self;
    self.squadTextField.delegate = self;
    self.militaryPressTextField.delegate = self;
    
    if ([CoreDataHelper loadInitialCapabilities].count!=0){
        InitialCapabilities *capabilities = [CoreDataHelper loadInitialCapabilities][0];
        if ([capabilities isKindOfClass:[CurrentCapabilities class]]){
            capabilities = [CoreDataHelper loadInitialCapabilities][1];
            self.initialCapabilities = capabilities;
        }else {
            self.initialCapabilities = capabilities;
        }
        NSLog(@"Exercises from initialCapForConfiguration:");
        for (Exercise *exercise in self.initialCapabilities.initialBestExercises) {
            NSLog(@"weight:%d type:%@ reps:%d",exercise.weight , exercise.type , exercise.reps );
        }
        [self configureTextFieldsIfInitialCapabilitiesTakePlace];
        
        [self.titleOwnHeightTextField  setEnabled:NO];
        [self.titleOwnWeightTextField setEnabled:NO];
        [self.titleBenchPressTextField setEnabled:NO];
        [self.titileDeadliftTextField setEnabled:NO];
        [self.titleSqudTextField setEnabled:NO];
        [self.titleMilitaryPressTextField setEnabled:NO];
        [self.titleStrengthCapabilities setEnabled:NO];

        [self performSegueWithIdentifier:@"pushToCreatedProgram" sender:self];
    }
}

- (void)configureTextFieldsIfInitialCapabilitiesTakePlace{
    for(Exercise *exercise in self.initialCapabilities.initialBestExercises){
        if ([exercise.type isEqualToString:gBenchPress]){
            self.benchPressTextField.text = [[NSNumber numberWithInt: exercise.weight] stringValue];
        }else if ([exercise.type isEqualToString:gDeadlift]){
            self.deadliftTextField.text = [[NSNumber numberWithInt:exercise.weight] stringValue];
        }else if ([exercise.type isEqualToString:gSquad]){
            self.squadTextField.text = [[NSNumber numberWithInt:exercise.weight] stringValue];
        }else if ([exercise.type isEqualToString:gMilitaryPress]){
            self.militaryPressTextField.text = [[NSNumber numberWithInt:exercise.weight] stringValue];
        }
    }
    self.ownHeightTextField.text = [[NSNumber numberWithInt: self.initialCapabilities.ownHeight] stringValue];
    self.ownWeightTextField.text = [[NSNumber numberWithInt: self.initialCapabilities.ownWeight] stringValue];
}

#pragma mark - Button Actions
- (IBAction)didTapReset:(id)sender{
    [CoreDataHelper clearChosenEntity:@"InitialCapabilities"];
}

- (IBAction)didTapDoneButton :(id)sender{
    if ([CoreDataHelper loadInitialCapabilities].count==0){
        [self saveInitialCapabilities];
        [self saveDataToFireBase];
    }
    [self performSegueWithIdentifier:@"pushToCreatedProgram" sender:self];
    
}

#pragma mark - Data Saving
- (void)saveInitialCapabilities{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    Exercise *exerciseBenchPress = [CoreDataHelper createExerciseInContext:context
                                                                          :kNumberOfRepsForCapabilityDetermination
                                                                          :[[self.storedData valueForKey:gBenchPress] intValue]
                                                                          :gBenchPress];
    Exercise *exerciseDeadlift = [CoreDataHelper createExerciseInContext:context
                                                                        :kNumberOfRepsForCapabilityDetermination
                                                                        :[[self.storedData valueForKey:gDeadlift] intValue]
                                                                        :gDeadlift];
    Exercise *exerciseSquad = [CoreDataHelper createExerciseInContext:context
                                                                     :kNumberOfRepsForCapabilityDetermination
                                                                     :[[self.storedData valueForKey:gSquad] intValue]
                                                                     :gSquad];
    Exercise *exerciseMilitary = [CoreDataHelper createExerciseInContext:context
                                                                        :kNumberOfRepsForCapabilityDetermination
                                                                        :[[self.storedData valueForKey:gMilitaryPress] intValue]
                                                                        :gMilitaryPress];
    
    int ownHeight = [[self.storedData valueForKey:gOwnHeight] intValue];
    int ownWeight = [[self.storedData valueForKey:gOwnWeight] intValue];
    
    NSMutableArray <Exercise*> *exerciseArray = [NSMutableArray new];
    [exerciseArray addObject: exerciseBenchPress];
    [exerciseArray addObject: exerciseDeadlift];
    [exerciseArray addObject: exerciseSquad];
    [exerciseArray addObject: exerciseMilitary];
    
    NSSet <Exercise*> *exercises = [[NSSet alloc] initWithArray: exerciseArray];
    NSLog(@"Exercises for save:");
    for (Exercise *exercise in exercises) {
        NSLog(@"weight:%d type:%@ reps:%d",exercise.weight , exercise.type , exercise.reps );
    }
    
    InitialCapabilities *initialCapabilities = [CoreDataHelper createInitialCapabilitiesInContext:context :exercises :ownWeight :ownHeight];
    NSLog(@"Exercises from saved initial capabilities:");
    for (Exercise *exercise in initialCapabilities.initialBestExercises) {
        NSLog(@"weight:%d type:%@ reps:%d",exercise.weight , exercise.type , exercise.reps );
    }
    
    [CoreDataHelper createCurrentCapabilitiesInContext:context :exercises :[NSSet new] :ownWeight :ownHeight];
    
    [appDelegate saveContext];
}


#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if ([textField isEqual: self.ownHeightTextField]){
        [textField resignFirstResponder];
        [self.storedData setValue:textField.text  forKey:gOwnHeight];  //saving of own height
        [self.ownWeightTextField becomeFirstResponder];
    }else if ([textField isEqual:self.ownWeightTextField]){
        [textField resignFirstResponder];
        [self.storedData setValue:textField.text forKey:gOwnWeight]; //saving of own weight
        [self.benchPressTextField becomeFirstResponder];
    }else if ([textField isEqual:self.benchPressTextField]){
        [textField resignFirstResponder];
        [self.storedData setValue:textField.text forKey:gBenchPress]; //saving of bench press
        [self.deadliftTextField becomeFirstResponder];
    }else if ([textField isEqual:self.deadliftTextField]){
        [textField resignFirstResponder];
        [self.storedData setValue:textField.text forKey:gDeadlift]; //saving of deadlift
        [self.squadTextField becomeFirstResponder];
    }else if ([textField isEqual:self.squadTextField]){
        [textField resignFirstResponder];
        [self.storedData setValue:textField.text forKey:gSquad]; //saving of squad
        [self.militaryPressTextField becomeFirstResponder];
    }else {
        [textField resignFirstResponder];
        [self.storedData setValue:textField.text forKey:gMilitaryPress]; //saving of military press
    }
    
    return YES;
}

#pragma mark - TextFields End Editing Actions
- (IBAction)didEndEditingHeight:(id)sender {
    [self.storedData setValue:self.ownHeightTextField.text  forKey:gOwnHeight];
    NSLog(@"%@height", self.ownHeightTextField.text);
}

- (IBAction)didEndEditingWeight:(id)sender {
    [self.storedData setValue:self.ownWeightTextField.text  forKey:gOwnWeight];
    NSLog(@"%@weight", self.ownWeightTextField.text);
}

- (IBAction)didEndEditingBenchPress:(id)sender {
    [self.storedData setValue:self.benchPressTextField.text forKey:gBenchPress];
    NSLog(@"%@benchPress", self.benchPressTextField.text);
}

- (IBAction)didEndEditingDeadlift:(id)sender {
    [self.storedData setValue:self.deadliftTextField.text forKey:gDeadlift];
    NSLog(@"%@deadlift", self.deadliftTextField.text);
}

- (IBAction)didEndEditingSquat:(id)sender {
    [self.storedData setValue:self.squadTextField.text forKey:gSquad];
    NSLog(@"%@squat", self.squadTextField.text);
}

- (IBAction)didEndEditingMilitaryPress:(id)sender {
    [self.storedData setValue:self.militaryPressTextField.text forKey:gMilitaryPress];
    NSLog(@"%@military", self.militaryPressTextField.text);
}

#pragma mark - Saving Data to Firebase
- (void)saveDataToFireBase{
    [[FIRAuth auth]
     signInAnonymouslyWithCompletion:^(FIRUser *_Nullable user, NSError *_Nullable error) {
         if (!error) {
             self.uid = user.uid;
             _ref=[[FIRDatabase database]reference];
             [self writeNewInitialCapabilitiesPost];
             [self writeNewCurrentCapabilitiesPost];
         }else {
             
         }
     }];
    
}

- (void)writeNewInitialCapabilitiesPost{
    NSString *key = self.uid;
    
    NSDictionary *post = @{gOwnWeight: self.ownWeightTextField.text,
                                                     gOwnHeight: self.ownHeightTextField.text,
                                                     gBenchPress: self.benchPressTextField.text,
                                                     gDeadlift: self.deadliftTextField.text,
                                                     gSquad: self.squadTextField.text,
                                                     gMilitaryPress: self.militaryPressTextField.text};
    NSDictionary *childUpdates = @{[NSString stringWithFormat:@"/user/%@/%@", key,@"Initial Capabilities"]: post};
    
    [_ref updateChildValues:childUpdates];
}

- (void)writeNewCurrentCapabilitiesPost{
    NSString *key = self.uid;
    
    NSDictionary *post = @{gOwnWeight: self.ownWeightTextField.text,
                                                     gOwnHeight: self.ownHeightTextField.text,
                                                     gBenchPress: self.benchPressTextField.text,
                                                     gDeadlift: self.deadliftTextField.text,
                                                     gSquad: self.squadTextField.text,
                                                     gMilitaryPress: self.militaryPressTextField.text};
    NSDictionary *childUpdates = @{[NSString stringWithFormat:@"/user/%@/%@", key,@"Current Capabilities"]: post};
    
    [_ref updateChildValues:childUpdates];
}
@end
