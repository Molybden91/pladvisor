//
//  TrainingDayViewingViewController.h
//  PLAdvisor
//
//  Created by Admin on 18.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainingDay+CoreDataClass.h"

@interface TrainingDayViewingViewController : UIViewController

@property (nonatomic, strong) TrainingDay *trainingDay;

@end
