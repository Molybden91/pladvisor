//
//  ProblemNotificationViewController.m
//  PLAdvisor
//
//  Created by Admin on 07.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ProblemNotificationViewController.h"
#import "ProblemNotificationTableViewCell.h"
#import "RightNavigationButton.h"

#import "CoreDataHelper.h"
#import "CurrentCapabilities+CoreDataClass.h"
#import "Injury+CoreDataClass.h"

#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>

static NSString *const kProblemNotificationID = @"ProblemNotificationID";

@interface ProblemNotificationViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property NSString *uid;

@end

@implementation ProblemNotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureRightNavigationButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UI Elements Configuration
- (void)configureRightNavigationButton{
    RightNavigationButton *doneButton = [RightNavigationButton new];
    doneButton = (RightNavigationButton *)[doneButton configureButton:@"DONE"];
    [doneButton addTarget:self action:@selector(didTapDoneButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:doneButton];
}

#pragma mark - Button Action
-(IBAction)didTapDoneButton:(id)sender{
    [self saveInjury];
    [self performSegueWithIdentifier:@"pushToInitialScreenFromProblem" sender:self];
}

#pragma mark - Saving Injury to Current Capabilities
- (void)saveInjury{
    NSArray <CurrentCapabilities*>* capabilitiesArray =[CoreDataHelper loadCurrentCapabilities];
    if (capabilitiesArray.count!=0){
        CurrentCapabilities *currentCapabilities = capabilitiesArray[0];
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
        ProblemNotificationTableViewCell *cell =[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM\\dd\\yyyy"];
    
        if ([cell.dropDownLabel.text isEqualToString:@"Injury"]){
            NSMutableArray <Exercise*>* arrayExercises = [NSMutableArray new];
            for (Exercise *exercise in currentCapabilities.currentBestExercises) {
                Exercise *exerciseCurrent = [CoreDataHelper createExerciseInContext:context :exercise.reps :exercise.weight :exercise.type];
                [arrayExercises addObject: exerciseCurrent];
            }
            NSSet <Exercise*>* exercises = [[NSSet alloc] initWithArray:arrayExercises];
            int ownHeight = currentCapabilities.ownHeight;
            int ownWeight = currentCapabilities.ownWeight;
        
            NSMutableArray <Injury*>* arrayInjury = [NSMutableArray new];
            for (Injury *injury in currentCapabilities.injuriesList) {
                Injury *injuryCurrent = [CoreDataHelper createInjuryInContext:context :injury.injuryType :injury.problemDate];
                [arrayInjury addObject:injuryCurrent];
            }
        
            Injury *injury = [CoreDataHelper createInjuryInContext:context :cell.concreteDropDownLabel.text :[formatter stringFromDate: cell.dateOfProblem.date ]];
            [arrayInjury addObject: injury];
            NSSet <Injury*>* injuries = [[NSSet alloc] initWithArray:arrayInjury];
        
            for (Injury *injury in injuries) {
                [self saveDataToFireBase:injury];
            }
        
        [CoreDataHelper clearChosenEntity:@"CurrentCapabilities"];
        [CoreDataHelper createCurrentCapabilitiesInContext:context :exercises :injuries :ownWeight :ownHeight];
        [appDelegate saveContext];
    }

    }
}

#pragma mark - Extracting Data from Cells
-(NSArray *)cellsForTableView:(UITableView *)tableView{
    NSInteger sections = tableView.numberOfSections;
    NSMutableArray *cells = [[NSMutableArray alloc]  init];
    for (int section = 0; section < sections; section++) {
        NSInteger rows =  [tableView numberOfRowsInSection:section];
        for (int row = 0; row < rows; row++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];   
            [cells addObject:cell];
        }
    }
    return cells;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ProblemNotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kProblemNotificationID forIndexPath:indexPath];
    return  cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  1;
}

#pragma mark - Saving Data to FireBase
- (void)saveDataToFireBase: (Injury *)injury{
    
    [[FIRAuth auth]
     signInAnonymouslyWithCompletion:^(FIRUser *_Nullable user, NSError *_Nullable error) {
         if (!error) {
             self.uid = user.uid;
             _ref=[[FIRDatabase database]reference];
             [self writeNewCurrentCapabilitiesPost:(Injury *)injury];
         }else {
             NSLog(@"error");
         }
     }];
}

- (void)writeNewCurrentCapabilitiesPost :(Injury *)injury {
    
    NSString *key = self.uid;
    
    NSDictionary *post = @{injury.problemDate : injury.injuryType};
    NSDictionary *childUpdates = @{[NSString stringWithFormat:@"/user/%@/%@/%@", key,@"Current Capabilities",@"Injuries"]: post};
    
    [_ref updateChildValues:childUpdates];
}

@end
