//
//  TrainingDayViewingViewController.m
//  PLAdvisor
//
//  Created by Admin on 18.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "TrainingDayViewingViewController.h"
#import "TrainingDayTableViewCell.h"


static  NSString *const kTrainingID = @"trainingDayID";
@interface TrainingDayViewingViewController () <UITableViewDataSource>

@end

@implementation TrainingDayViewingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.trainingDay.trainingDayExercises.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TrainingDayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTrainingID forIndexPath:indexPath];
    
    NSMutableArray *exercises= [NSMutableArray new];
    int i=0;
    
    for (Exercise *exercise in self.trainingDay.trainingDayExercises) {
        exercises[i] =exercise;
        i++;
    }
    [cell configureTrainingDayCell:exercises[indexPath.row]];
    return cell;
}

@end
