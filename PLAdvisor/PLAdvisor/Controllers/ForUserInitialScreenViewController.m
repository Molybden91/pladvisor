//
//  InitialScreenViewController.m
//  PLAdvisor
//
//  Created by Admin on 21.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ForUserInitialScreenViewController.h"
#import <Firebase/Firebase.h>
#import <FirebaseDatabase/FirebaseDatabase.h>

@interface ForUserInitialScreenViewController () <UITextFieldDelegate>

@property (strong, nonatomic) FIRDatabaseReference *ref;
@property(strong, nonatomic) FIRAuthStateDidChangeListenerHandle handle;

@end

@implementation ForUserInitialScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.ref = [[FIRDatabase database] reference];
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    self.handle = [[FIRAuth auth] addAuthStateDidChangeListener:^(FIRAuth * _Nonnull auth, FIRUser * _Nullable user) {
    }];
}

- (void)viewWillDisappear:(BOOL)animated{
    [[FIRAuth auth] removeAuthStateDidChangeListener:_handle];
}

#pragma mark - Working with Users
- (void)createUser: (NSString *) email : (NSString *) password {
    [[FIRAuth auth] createUserWithEmail:email password:password completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
        if (error == nil){
            NSLog(@"%@ created", user.email);
        }
    }];
}

- (void)signIn: (NSString *) email : (NSString *) password{
    [[FIRAuth auth] signInWithEmail:email password:password completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
        if (error==nil){
            NSLog(@"signing in user with email: %@",user.email);
        }
    }];
}

#pragma mark - Button Actions
- (IBAction)didTapSignUp:(id)sender {
    [self createUser:self.emailTextField.text :self.passwordTextField.text];
}

- (IBAction)didTapSignIn:(id)sender {
    [self signIn:self.emailTextField.text :self.passwordTextField.text];
    [self performSegueWithIdentifier:@"toStartScreen" sender:self];
}

#pragma mark - TextField Begin Editing Actions
- (IBAction)didEditingEmailBegin:(id)sender {
    self.emailTextField.text= @"";
    self.emailTextField.textColor = [UIColor blackColor];
}

- (IBAction)didEditingPasswordBegin:(id)sender {
    self.passwordTextField.text = @"";
    self.passwordTextField.textColor = [UIColor blackColor];
}

@end
