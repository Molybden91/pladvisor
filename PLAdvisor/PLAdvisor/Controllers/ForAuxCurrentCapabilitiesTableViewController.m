//
//  ForAuxCurrentCapabilitiesTableViewController.m
//  PLAdvisor
//
//  Created by Admin on 26.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ForAuxCurrentCapabilitiesTableViewController.h"
#import "AuxCurrentCapabilitiesTableViewCell.h"

#import "Injury+CoreDataClass.h"
#import "CurrentCapabilities+CoreDataClass.h"
#import "CoreDataHelper.h"

#import "RightNavigationButton.h"

static  NSString *const  kExercise= @"exercisesID";

@interface ForAuxCurrentCapabilitiesTableViewController () <UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *ownWeightLabel;
@property (weak, nonatomic) IBOutlet UITextField *ownWeightTextField;
@property (weak, nonatomic) IBOutlet UILabel *ownHeightLabel;
@property (weak, nonatomic) IBOutlet UITextField *ownHeightTextField;

@property (weak, nonatomic) IBOutlet UITextView *injuryTypeTextView;
@property (weak, nonatomic) IBOutlet UITextView *dateOfInjuryTextView;

@property (weak, nonatomic) IBOutlet UILabel *injuryTypelabel;
@property (weak, nonatomic) IBOutlet UILabel *dateOfInjuryLabel;

@property (nonatomic,strong) CurrentCapabilities *currentCapabilities;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ForAuxCurrentCapabilitiesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureLabels];
    [self prepareCurrentCapabilities];
    [self configureTextFields];
    [self configureRightNavigationButton];
    self.tableView.estimatedRowHeight = 100;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark - UI Elements Configuration
- (void)configureLabels{
    self.ownHeightLabel.enabled = NO;
    self.ownWeightLabel.enabled = NO;
    self.injuryTypelabel.enabled = NO;
    self.dateOfInjuryLabel.enabled = NO;
}

- (void)configureTextFields{
    
    if (self.currentCapabilities!=nil){
        self.ownWeightTextField.text = [[NSNumber numberWithInt: self.currentCapabilities.ownWeight ] stringValue];
        self.ownHeightTextField.text = [[NSNumber numberWithInt:self.currentCapabilities.ownHeight] stringValue];
        NSString *preparedInjuries = @"";
        NSString *preparedDates = @"";
        NSLog(@"%ld", self.currentCapabilities.injuriesList.count);
    
        for (Injury *injury in self.currentCapabilities.injuriesList) {
            preparedInjuries = [NSString stringWithFormat:@"%@%@%s",preparedInjuries,injury.injuryType,"\n"];
        
            preparedDates = [NSString stringWithFormat:@"%@%@%s", preparedDates, injury.problemDate,"\n"];
        }
    
        self.injuryTypeTextView.text = preparedInjuries;
        self.dateOfInjuryTextView.text = preparedDates;
    
        [self.ownHeightTextField setEnabled:NO];
        [self.ownWeightTextField setEnabled:NO];
        [self.injuryTypeTextView setEditable:NO];
        [self.dateOfInjuryTextView setEditable:NO];
    }
}

- (void)configureRightNavigationButton{
    RightNavigationButton *doneButton = [RightNavigationButton new];
    doneButton = (RightNavigationButton *)[doneButton configureButton:@"DONE"];
    [doneButton addTarget:self action:@selector(didTapDoneButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:doneButton];
}

#pragma mark - Data Preparation
- (void)prepareCurrentCapabilities{
    NSArray <CurrentCapabilities*> *capabilities= [CoreDataHelper loadCurrentCapabilities];
    if(capabilities.count!=0){
        self.currentCapabilities = capabilities[0];
    }
}

#pragma mark - Button Actions
- (IBAction)didTapDoneButton:(id)sender{
    [self performSegueWithIdentifier:@"fromCurrentCapabilitiesToInitialTransition" sender:self];
}
#pragma mark - TableView data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.currentCapabilities.currentBestExercises.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AuxCurrentCapabilitiesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kExercise forIndexPath:indexPath];
    NSMutableArray *bestExercises =[NSMutableArray new];
    for (Exercise *exercise in self.currentCapabilities.currentBestExercises) {
        [bestExercises addObject:exercise];
    }
    [cell configureCell:bestExercises[indexPath.row]];
    
    
    return cell;
}

@end
