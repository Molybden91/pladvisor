//
//  ExerciseAdditingViewController.m
//  PLAdvisor
//
//  Created by Admin on 06.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ExerciseAdditingViewController.h"
#import "ExerciseAdditingTableViewCell.h"
#import "RightNavigationButton.h"

#import "TrainingDay+CoreDataClass.h"
#import "CoreDataHelper.h"

#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>

static NSString* const kExerciseAdditingCell = @"ExerciseAdditingID";

@interface ExerciseAdditingViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, ExerciseAdditingTableViewCellDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIDatePicker *dateOfExecution;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) TrainingDay *trainingDay;
@property (nonatomic, strong) NSMutableArray<Exercise*> *exercises;

@property (strong, nonatomic) FIRDatabaseReference *ref;
@property NSString *uid;

@end

@implementation ExerciseAdditingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setEditing:YES animated:YES];
    self.exercises = [NSMutableArray new];
    [self configureTrainingDay];
    [self configureRightNavigationButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Configuration
- (void)configureTrainingDay{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    [self.exercises  addObject:[CoreDataHelper createExerciseInContext:context :0 :0 :@""]];
}

- (void)configureRightNavigationButton{
    RightNavigationButton *doneButton = [RightNavigationButton new];
    doneButton = (RightNavigationButton *)[doneButton configureButton:@"DONE"];
    [doneButton addTarget:self action:@selector(didTapDoneButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:doneButton];
}

#pragma mark - Button Action
- (IBAction)didTapDoneButton :(id)sender{
    [self saveDataFromCells];
    [self checkIfRecord:self.exercises];
    [self saveExercises];
    
    [self performSegueWithIdentifier:@"pushToInitialScreenFromProgramCreation" sender:self];
}

#pragma mark - Extracting Data from Cell
-(NSArray *)cellsForTableView:(UITableView *)tableView{
    NSInteger sections = tableView.numberOfSections;
    NSMutableArray *cells = [[NSMutableArray alloc]  init];
    for (int section = 0; section < sections; section++) {
        NSInteger rows =  [tableView numberOfRowsInSection:section];
        for (int row = 0; row < rows; row++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [cells addObject:cell];
        }
    }
    return cells;
}

#pragma mark - Data Saving
- (void)saveDataFromCells{
    NSArray *cells = [self cellsForTableView:self.tableView];
    NSMutableArray *auxCellsArray = [cells mutableCopy];
    
    for (int i = 0; i<auxCellsArray.count;i++){
        ExerciseAdditingTableViewCell *cell = auxCellsArray[i];
        [CoreDataHelper redefineExercise:[self.exercises objectAtIndex:i] :[cell.repsTextField.text intValue] :[cell.weightTextField.text intValue] :cell.exercisesLabel.text];
    }
}

- (void)saveExercises{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSLog(@"%@",appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.firstObject );
    
    NSSet *exercisesForSending = [NSSet setWithArray:self.exercises];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    
    [CoreDataHelper createTrainingDayInContext:context :exercisesForSending :[formatter stringFromDate: self.dateOfExecution.date]];
    NSLog(@"%@",[formatter stringFromDate: self.dateOfExecution.date ]);
    
    [appDelegate saveContext];
}

- (void)checkIfRecord:(NSMutableArray<Exercise*> *)savedExercises{
    for (int i = 0; i<savedExercises.count;i++){
        [self saveDataToFireBase:i];
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
        
        CurrentCapabilities *capabilities = [CoreDataHelper loadCurrentCapabilities][0];
        NSSet <Exercise*>* exercises= [NSSet setWithSet: capabilities.currentBestExercises ];
        
        NSSet <Injury*>*injuries = capabilities.injuriesList;
        int ownWeight = capabilities.ownWeight;
        int ownHeight = capabilities.ownHeight;
        
        for (Exercise *exercise in exercises) {
            if (savedExercises[i].weight> exercise.weight){
                if ([savedExercises[i].type isEqualToString:exercise.type]){
                    exercise.weight=savedExercises[i].weight;
                }
            }
        }
        [CoreDataHelper clearChosenEntity:@"CurrentCapabilities"];
        [CoreDataHelper createCurrentCapabilitiesInContext:context :exercises :injuries :ownWeight :ownHeight];
    }
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.exercises.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ExerciseAdditingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kExerciseAdditingCell forIndexPath:indexPath];
    cell.delegate = self;
    cell.repsTextField.delegate = self;
    cell.weightTextField.delegate = self;
    return cell;
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    [self.exercises exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
}

#pragma mark - UITableViewDelegate

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"Delete";
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        return UITableViewCellEditingStyleInsert;
    }else {
        return UITableViewCellEditingStyleDelete;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete){
        [self.exercises removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }else if (editingStyle == UITableViewCellEditingStyleInsert){
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
        Exercise *exerciseForWrite = [CoreDataHelper createExerciseInContext:context :0 :0 :@""];
        [self.exercises insertObject:exerciseForWrite atIndex:0];
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Saving Data to FireBase
-(void)saveAllDataToFireBase{
    for(int i=0;i<self.exercises.count;i++){
        [self saveDataToFireBase:i];
    }
}

- (void)saveDataToFireBase :(int)numberOfExercise{
    
    [[FIRAuth auth]
     signInAnonymouslyWithCompletion:^(FIRUser *_Nullable user, NSError *_Nullable error) {
         if (!error) {
             self.uid = user.uid;
             _ref=[[FIRDatabase database]reference];
             [self writeNewCurrentCapabilitiesPost:(int)numberOfExercise];
         }else {
             NSLog(@"error");
         }
     }];
}

- (void)writeNewCurrentCapabilitiesPost :(int)numberOfExercise  {
    
    NSString *key = self.uid;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM\\dd\\yyyy"];
    
    Exercise *currentExercise =self.exercises[numberOfExercise];
    NSDictionary *post = @{[[NSNumber numberWithInt: currentExercise.reps] stringValue]:[ NSNumber numberWithInt:currentExercise.weight]};
    NSDictionary *childUpdates = @{[NSString stringWithFormat:@"/user/%@/%@/%@/%@", key,@"Diary",[formatter stringFromDate: self.dateOfExecution.date],currentExercise.type]: post};
    
    [_ref updateChildValues:childUpdates];
}

@end
