//
//  AnalyzeDataViewController.h
//  PLAdvisor
//
//  Created by Admin on 07.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnalyzeDataViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *AnalyzeDataTextField;
@property (weak, nonatomic) IBOutlet UIButton *dropDownButton;
@property (weak, nonatomic) IBOutlet UIButton *monthSelectionButton;
@property (weak, nonatomic) IBOutlet UIButton *yearSelectionButton;
@property (weak, nonatomic) IBOutlet UIButton *allTimeselectionButton;
@property (weak, nonatomic) IBOutlet UILabel *dropDownLabel;


@end
