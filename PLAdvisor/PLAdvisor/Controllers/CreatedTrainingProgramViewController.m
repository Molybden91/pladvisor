//
//  CreatedTrainingProgramViewController.m
//  PLAdvisor
//
//  Created by Admin on 06.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "CreatedTrainingProgramViewController.h"
#import "CreatedTrainigProgramTableViewCell.h"
#import "RightNavigationButton.h"
#import "CentralNavigationButton.h"

#import "InitialCapabilities+CoreDataClass.h"
#import "Exercise+CoreDataClass.h"
#import "CurrentProgram+CoreDataClass.h"
#import "TrainingDay+CoreDataClass.h"
#import "CurrentProgram+CoreDataClass.h"
#import "AppDelegate.h"
#import "CoreDataHelper.h"
#import "Constants.h"
#import "ProgramCreationHelper.h"

static NSString* const kCreatedProgramCellID= @"CreatedProgramCellID";
NSInteger const kNumberOftrainigDays = 12;

@interface CreatedTrainingProgramViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray <NSMutableArray *>* preparedExercises;
@property (nonatomic, assign) NSInteger numberOfTrainingDays;

@end

@implementation CreatedTrainingProgramViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configurateTableView];
    [self configureRightNavigationButton];
    [self configureTitleNavigationButton];
    [self trainingProgramCalculation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark - UI Configuration
- (void)configurateTableView{    
    self.tableView.sectionHeaderHeight = 44;
    self.numberOfTrainingDays = 12;
    self.preparedExercises = [NSMutableArray new];
}

- (void)configureRightNavigationButton{
    RightNavigationButton *doneButton = [RightNavigationButton new];
    doneButton = (RightNavigationButton *)[doneButton configureButton:@"DONE"];
    [doneButton addTarget:self action:@selector(didTapDoneButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:doneButton];
}

- (void)configureTitleNavigationButton{
    CentralNavigationButton *titleButton = [CentralNavigationButton new];
    titleButton = (CentralNavigationButton *)[titleButton configureButton:@"TO DIARY"];
    [titleButton addTarget:self action:@selector(didTapToDiaryButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleButton;
}

#pragma mark - Button Actions
- (IBAction)didTapDoneButton:(id)sender{
    [self performSegueWithIdentifier:@"toInitialScreenTransition" sender:self];
}

- (IBAction)didTapToDiaryButton:(id)sender{
    [self performSegueWithIdentifier:@"toDiaryTransition" sender:self];
}

#pragma mark - Training Program Preparation
-(void)trainingProgramCalculation{
    NSArray *program = [CoreDataHelper loadCurrentProgram];
    ProgramCreationHelper *helper = [ProgramCreationHelper new];
    if (program.count==0){
        self.preparedExercises = [helper  trainingProgramCalculation];
    }else {
        self.preparedExercises = [helper prepareExistenceProgram:program];
    }
}

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.preparedExercises.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *aux = [NSString stringWithFormat:@"%@%ld",@"Day", (long)section+1];
    return aux;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CreatedTrainigProgramTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCreatedProgramCellID forIndexPath:indexPath];
    NSArray *section = self.preparedExercises[indexPath.section];
    [cell configureCell:section];
    return cell;
}


@end
