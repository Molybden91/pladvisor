//
//  ViewController.m
//  PLAdvisor
//
//  Created by Admin on 04.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "InitialScreenViewController.h"
#import "CentralNavigationButton.h"
#import "RightNavigationButton.h"
#import "CoreDataHelper.h"
#import "CurrentCapabilities+CoreDataClass.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import "Constants.h"
#import "CapabilitiesMapper.h"
#import "DiaryMapper.h"

@import Firebase;

@interface InitialScreenViewController ()

@property (strong, nonatomic) FIRDatabaseReference *ref;
@property NSString *uid;
@property (strong, nonatomic) NSString *weight;
@property (strong, nonatomic) CurrentCapabilities *currentCapabilities;

@end


@implementation InitialScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.ref = [[FIRDatabase database] reference];
    [self loadUserData];
    [self configureRightNavigationButton];
    [self configureTitleNavigationButton];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Configuration
- (void)configureRightNavigationButton{
    RightNavigationButton *doneButton = [RightNavigationButton new];
    doneButton = (RightNavigationButton *)[doneButton configureButton:@"WEIGHT"];
    [doneButton addTarget:self action:@selector(didTapResetButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:doneButton];
}

- (void)configureTitleNavigationButton{
    CentralNavigationButton *titleButton = [CentralNavigationButton new];
    titleButton = (CentralNavigationButton *)[titleButton configureButton:@"SEE RECORD"];
    [titleButton addTarget:self action:@selector(didTapRecord:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleButton;
}

- (UIAlertController *)alertControllerConfiguration{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Weight"
                                                                              message: @"Enter your new weight"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"weight";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    
    return alertController;
}

#pragma mark - Button Actions
- (IBAction)didTapRecord:(id)sender{
    [self performSegueWithIdentifier:@"toRecordTransition" sender:self];
    //[self saveDataToFireBase:self.weight];
}

- (IBAction)didTapResetButton:(id)sender{
    UIAlertController * alertController = [self alertControllerConfiguration];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * weightField = textfields[0];
        
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
        
        CurrentCapabilities *capabilities = [CoreDataHelper loadCurrentCapabilities][0];
        if (capabilities!=nil){
            self.currentCapabilities = capabilities;
            
            NSMutableArray <Exercise*>* arrayExercises = [NSMutableArray new];
            for (Exercise *exercise in self.currentCapabilities.currentBestExercises) {
                Exercise *exerciseCurrent = [CoreDataHelper createExerciseInContext:context :exercise.reps :exercise.weight :exercise.type];
                [arrayExercises addObject: exerciseCurrent];
            }
            NSSet <Exercise*>* exercises = [[NSSet alloc] initWithArray:arrayExercises];
            int ownHeight = capabilities.ownHeight;
            NSSet <Injury*>* injuries = capabilities.injuriesList;
        
            self.weight= weightField.text;
            [CoreDataHelper clearChosenEntity:@"CurrentCapabilities"];
            [CoreDataHelper createCurrentCapabilitiesInContext:context :exercises :injuries :[weightField.text  intValue] :ownHeight];
            
            
            [appDelegate saveContext];
        
        }
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Saving Data to FireBase
- (void)forFireBaseSaving{
    CurrentCapabilities *capabilities = [CoreDataHelper loadCurrentCapabilities][0];
    if (capabilities!=nil){
        self.currentCapabilities = capabilities;
    }
    [self saveDataToFireBase:self.weight];
}


- (void)saveDataToFireBase: (NSString*)weight{

    [[FIRAuth auth]
     signInAnonymouslyWithCompletion:^(FIRUser *_Nullable user, NSError *_Nullable error) {
         if (!error) {
             self.uid = user.uid;
             _ref=[[FIRDatabase database]reference];
             [self writeNewCurrentCapabilitiesPost:weight];
         }else {
             NSLog(@"error");
         }
     }];
}

- (void)writeNewCurrentCapabilitiesPost :(NSString*)weight {

    NSString *key = self.uid;
    
    NSMutableDictionary <NSString*, NSNumber*>* exercisesDictionary= [CoreDataHelper setOfExercisesToDictionary:self.currentCapabilities.currentBestExercises];
    NSDictionary *post = @{gOwnWeight: weight,
                           gOwnHeight: [NSNumber numberWithInt: self.currentCapabilities.ownHeight],
                           gBenchPress: [exercisesDictionary valueForKey:gBenchPress],
                           gDeadlift: [exercisesDictionary valueForKey:gDeadlift],
                           gSquad: [exercisesDictionary valueForKey:gSquad],
                           gMilitaryPress: [exercisesDictionary valueForKey:gMilitaryPress]};
    NSDictionary *childUpdates = @{[NSString stringWithFormat:@"/user/%@/%@", key,@"Current Capabilities"]: post};
    
    [_ref updateChildValues:childUpdates];
}

#pragma mark - Loading Data From Firebase
- (void)loadUserData{
    NSArray *capabilities = [CoreDataHelper loadCurrentCapabilities];
    if (capabilities.count==0) {
        NSString *userID = [FIRAuth auth].currentUser.uid;
        [[[_ref child:@"user"] child:userID] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if (snapshot.exists){
                [self writeDataFromFireBaseToCoreData:snapshot.value];
                NSLog(@"%@%@", snapshot.key,snapshot.value);
            }
        } withCancelBlock:^(NSError * _Nonnull error) {
            NSLog(@"%@", error.localizedDescription);
        }];
    }
}

- (void)writeDataFromFireBaseToCoreData:(NSDictionary *) json{
    [CapabilitiesMapper modelFromJson:json:@"Current Capabilities"];
    [CapabilitiesMapper modelFromJson:json:@"Initial Capabilities"];
    [DiaryMapper modelFromJson:json];
}

#pragma mark - Action For Button To Clear CoreData
- (IBAction)clearAll:(id)sender {
    [CoreDataHelper clearChosenEntity:@"Exercise"];
    [CoreDataHelper clearChosenEntity:@"Injury"];
    [CoreDataHelper clearChosenEntity:@"InitialCapabilities"];
    [CoreDataHelper clearChosenEntity:@"CurrentCapabilities"];
    [CoreDataHelper clearChosenEntity:@"TrainingDay"];
    [CoreDataHelper clearChosenEntity:@"CurrentProgram"];
}
@end
